import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'user-add',
        data: { pageTitle: 'pmsApp.userAdd.home.title' },
        loadChildren: () => import('./user-add/user-add.module').then(m => m.UserAddModule),
      },
      {
        path: 'organization',
        data: { pageTitle: 'pmsApp.organization.home.title' },
        loadChildren: () => import('./organization/organization.module').then(m => m.OrganizationModule),
      },
      {
        path: 'groups',
        data: { pageTitle: 'pmsApp.groups.home.title' },
        loadChildren: () => import('./groups/groups.module').then(m => m.GroupsModule),
      },
      {
        path: 'service',
        data: { pageTitle: 'pmsApp.service.home.title' },
        loadChildren: () => import('./service/service.module').then(m => m.ServiceModule),
      },
      {
        path: 'payment',
        data: { pageTitle: 'pmsApp.payment.home.title' },
        loadChildren: () => import('./payment/payment.module').then(m => m.PaymentModule),
      },
      {
        path: 'payment-history',
        data: { pageTitle: 'pmsApp.paymentHistory.home.title' },
        loadChildren: () => import('./payment-history/payment-history.module').then(m => m.PaymentHistoryModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
