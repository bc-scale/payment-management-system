import { IUserAdd, NewUserAdd } from './user-add.model';

export const sampleWithRequiredData: IUserAdd = {
  id: 59800,
};

export const sampleWithPartialData: IUserAdd = {
  id: 60297,
  account: 71619,
};

export const sampleWithFullData: IUserAdd = {
  id: 54510,
  account: 43161,
};

export const sampleWithNewData: NewUserAdd = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
