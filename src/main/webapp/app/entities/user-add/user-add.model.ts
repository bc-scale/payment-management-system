import { IUser } from 'app/entities/user/user.model';
import { IGroups } from 'app/entities/groups/groups.model';

export interface IUserAdd {
  id: number;
  account?: number | null;
  user?: Pick<IUser, 'id'> | null;
  groups?: Pick<IGroups, 'id'>[] | null;
}

export type NewUserAdd = Omit<IUserAdd, 'id'> & { id: null };
