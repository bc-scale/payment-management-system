import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../user-add.test-samples';

import { UserAddFormService } from './user-add-form.service';

describe('UserAdd Form Service', () => {
  let service: UserAddFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserAddFormService);
  });

  describe('Service methods', () => {
    describe('createUserAddFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createUserAddFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            account: expect.any(Object),
            user: expect.any(Object),
            groups: expect.any(Object),
          })
        );
      });

      it('passing IUserAdd should create a new form with FormGroup', () => {
        const formGroup = service.createUserAddFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            account: expect.any(Object),
            user: expect.any(Object),
            groups: expect.any(Object),
          })
        );
      });
    });

    describe('getUserAdd', () => {
      it('should return NewUserAdd for default UserAdd initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createUserAddFormGroup(sampleWithNewData);

        const userAdd = service.getUserAdd(formGroup) as any;

        expect(userAdd).toMatchObject(sampleWithNewData);
      });

      it('should return NewUserAdd for empty UserAdd initial value', () => {
        const formGroup = service.createUserAddFormGroup();

        const userAdd = service.getUserAdd(formGroup) as any;

        expect(userAdd).toMatchObject({});
      });

      it('should return IUserAdd', () => {
        const formGroup = service.createUserAddFormGroup(sampleWithRequiredData);

        const userAdd = service.getUserAdd(formGroup) as any;

        expect(userAdd).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IUserAdd should not enable id FormControl', () => {
        const formGroup = service.createUserAddFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewUserAdd should disable id FormControl', () => {
        const formGroup = service.createUserAddFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
