import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { UserAddFormService, UserAddFormGroup } from './user-add-form.service';
import { IUserAdd } from '../user-add.model';
import { UserAddService } from '../service/user-add.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

@Component({
  selector: 'jhi-user-add-update',
  templateUrl: './user-add-update.component.html',
})
export class UserAddUpdateComponent implements OnInit {
  isSaving = false;
  userAdd: IUserAdd | null = null;

  usersSharedCollection: IUser[] = [];

  editForm: UserAddFormGroup = this.userAddFormService.createUserAddFormGroup();

  constructor(
    protected userAddService: UserAddService,
    protected userAddFormService: UserAddFormService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareUser = (o1: IUser | null, o2: IUser | null): boolean => this.userService.compareUser(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userAdd }) => {
      this.userAdd = userAdd;
      if (userAdd) {
        this.updateForm(userAdd);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const userAdd = this.userAddFormService.getUserAdd(this.editForm);
    if (userAdd.id !== null) {
      this.subscribeToSaveResponse(this.userAddService.update(userAdd));
    } else {
      this.subscribeToSaveResponse(this.userAddService.create(userAdd));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserAdd>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(userAdd: IUserAdd): void {
    this.userAdd = userAdd;
    this.userAddFormService.resetForm(this.editForm, userAdd);

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing<IUser>(this.usersSharedCollection, userAdd.user);
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing<IUser>(users, this.userAdd?.user)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));
  }
}
