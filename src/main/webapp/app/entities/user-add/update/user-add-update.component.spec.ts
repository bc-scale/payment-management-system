import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { UserAddFormService } from './user-add-form.service';
import { UserAddService } from '../service/user-add.service';
import { IUserAdd } from '../user-add.model';

import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

import { UserAddUpdateComponent } from './user-add-update.component';

describe('UserAdd Management Update Component', () => {
  let comp: UserAddUpdateComponent;
  let fixture: ComponentFixture<UserAddUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let userAddFormService: UserAddFormService;
  let userAddService: UserAddService;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [UserAddUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(UserAddUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(UserAddUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    userAddFormService = TestBed.inject(UserAddFormService);
    userAddService = TestBed.inject(UserAddService);
    userService = TestBed.inject(UserService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call User query and add missing value', () => {
      const userAdd: IUserAdd = { id: 456 };
      const user: IUser = { id: 62226 };
      userAdd.user = user;

      const userCollection: IUser[] = [{ id: 44634 }];
      jest.spyOn(userService, 'query').mockReturnValue(of(new HttpResponse({ body: userCollection })));
      const additionalUsers = [user];
      const expectedCollection: IUser[] = [...additionalUsers, ...userCollection];
      jest.spyOn(userService, 'addUserToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ userAdd });
      comp.ngOnInit();

      expect(userService.query).toHaveBeenCalled();
      expect(userService.addUserToCollectionIfMissing).toHaveBeenCalledWith(
        userCollection,
        ...additionalUsers.map(expect.objectContaining)
      );
      expect(comp.usersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const userAdd: IUserAdd = { id: 456 };
      const user: IUser = { id: 52394 };
      userAdd.user = user;

      activatedRoute.data = of({ userAdd });
      comp.ngOnInit();

      expect(comp.usersSharedCollection).toContain(user);
      expect(comp.userAdd).toEqual(userAdd);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IUserAdd>>();
      const userAdd = { id: 123 };
      jest.spyOn(userAddFormService, 'getUserAdd').mockReturnValue(userAdd);
      jest.spyOn(userAddService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ userAdd });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: userAdd }));
      saveSubject.complete();

      // THEN
      expect(userAddFormService.getUserAdd).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(userAddService.update).toHaveBeenCalledWith(expect.objectContaining(userAdd));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IUserAdd>>();
      const userAdd = { id: 123 };
      jest.spyOn(userAddFormService, 'getUserAdd').mockReturnValue({ id: null });
      jest.spyOn(userAddService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ userAdd: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: userAdd }));
      saveSubject.complete();

      // THEN
      expect(userAddFormService.getUserAdd).toHaveBeenCalled();
      expect(userAddService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IUserAdd>>();
      const userAdd = { id: 123 };
      jest.spyOn(userAddService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ userAdd });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(userAddService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareUser', () => {
      it('Should forward to userService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(userService, 'compareUser');
        comp.compareUser(entity, entity2);
        expect(userService.compareUser).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
