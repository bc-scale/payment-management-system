import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IUserAdd, NewUserAdd } from '../user-add.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IUserAdd for edit and NewUserAddFormGroupInput for create.
 */
type UserAddFormGroupInput = IUserAdd | PartialWithRequiredKeyOf<NewUserAdd>;

type UserAddFormDefaults = Pick<NewUserAdd, 'id' | 'groups'>;

type UserAddFormGroupContent = {
  id: FormControl<IUserAdd['id'] | NewUserAdd['id']>;
  account: FormControl<IUserAdd['account']>;
  user: FormControl<IUserAdd['user']>;
  groups: FormControl<IUserAdd['groups']>;
};

export type UserAddFormGroup = FormGroup<UserAddFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class UserAddFormService {
  createUserAddFormGroup(userAdd: UserAddFormGroupInput = { id: null }): UserAddFormGroup {
    const userAddRawValue = {
      ...this.getFormDefaults(),
      ...userAdd,
    };
    return new FormGroup<UserAddFormGroupContent>({
      id: new FormControl(
        { value: userAddRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      account: new FormControl(userAddRawValue.account),
      user: new FormControl(userAddRawValue.user),
      groups: new FormControl(userAddRawValue.groups ?? []),
    });
  }

  getUserAdd(form: UserAddFormGroup): IUserAdd | NewUserAdd {
    return form.getRawValue() as IUserAdd | NewUserAdd;
  }

  resetForm(form: UserAddFormGroup, userAdd: UserAddFormGroupInput): void {
    const userAddRawValue = { ...this.getFormDefaults(), ...userAdd };
    form.reset(
      {
        ...userAddRawValue,
        id: { value: userAddRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): UserAddFormDefaults {
    return {
      id: null,
      groups: [],
    };
  }
}
