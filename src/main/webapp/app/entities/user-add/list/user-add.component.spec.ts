import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { UserAddService } from '../service/user-add.service';

import { UserAddComponent } from './user-add.component';

describe('UserAdd Management Component', () => {
  let comp: UserAddComponent;
  let fixture: ComponentFixture<UserAddComponent>;
  let service: UserAddService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'user-add', component: UserAddComponent }]), HttpClientTestingModule],
      declarations: [UserAddComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(UserAddComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(UserAddComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(UserAddService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.userAdds?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to userAddService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getUserAddIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getUserAddIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});
