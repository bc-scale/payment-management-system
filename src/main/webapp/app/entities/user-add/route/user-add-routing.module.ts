import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { UserAddComponent } from '../list/user-add.component';
import { UserAddDetailComponent } from '../detail/user-add-detail.component';
import { UserAddUpdateComponent } from '../update/user-add-update.component';
import { UserAddRoutingResolveService } from './user-add-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const userAddRoute: Routes = [
  {
    path: '',
    component: UserAddComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: UserAddDetailComponent,
    resolve: {
      userAdd: UserAddRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: UserAddUpdateComponent,
    resolve: {
      userAdd: UserAddRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: UserAddUpdateComponent,
    resolve: {
      userAdd: UserAddRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(userAddRoute)],
  exports: [RouterModule],
})
export class UserAddRoutingModule {}
