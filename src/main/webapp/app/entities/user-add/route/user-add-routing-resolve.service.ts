import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IUserAdd } from '../user-add.model';
import { UserAddService } from '../service/user-add.service';

@Injectable({ providedIn: 'root' })
export class UserAddRoutingResolveService implements Resolve<IUserAdd | null> {
  constructor(protected service: UserAddService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUserAdd | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((userAdd: HttpResponse<IUserAdd>) => {
          if (userAdd.body) {
            return of(userAdd.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
