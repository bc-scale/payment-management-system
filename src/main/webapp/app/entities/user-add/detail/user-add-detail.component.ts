import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserAdd } from '../user-add.model';

@Component({
  selector: 'jhi-user-add-detail',
  templateUrl: './user-add-detail.component.html',
})
export class UserAddDetailComponent implements OnInit {
  userAdd: IUserAdd | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userAdd }) => {
      this.userAdd = userAdd;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
