import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { UserAddDetailComponent } from './user-add-detail.component';

describe('UserAdd Management Detail Component', () => {
  let comp: UserAddDetailComponent;
  let fixture: ComponentFixture<UserAddDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UserAddDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ userAdd: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(UserAddDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(UserAddDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load userAdd on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.userAdd).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
