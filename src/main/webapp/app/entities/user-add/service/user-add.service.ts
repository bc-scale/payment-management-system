import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IUserAdd, NewUserAdd } from '../user-add.model';

export type PartialUpdateUserAdd = Partial<IUserAdd> & Pick<IUserAdd, 'id'>;

export type EntityResponseType = HttpResponse<IUserAdd>;
export type EntityArrayResponseType = HttpResponse<IUserAdd[]>;

@Injectable({ providedIn: 'root' })
export class UserAddService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/user-adds');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(userAdd: NewUserAdd): Observable<EntityResponseType> {
    return this.http.post<IUserAdd>(this.resourceUrl, userAdd, { observe: 'response' });
  }

  update(userAdd: IUserAdd): Observable<EntityResponseType> {
    return this.http.put<IUserAdd>(`${this.resourceUrl}/${this.getUserAddIdentifier(userAdd)}`, userAdd, { observe: 'response' });
  }

  partialUpdate(userAdd: PartialUpdateUserAdd): Observable<EntityResponseType> {
    return this.http.patch<IUserAdd>(`${this.resourceUrl}/${this.getUserAddIdentifier(userAdd)}`, userAdd, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUserAdd>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUserAdd[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getUserAddIdentifier(userAdd: Pick<IUserAdd, 'id'>): number {
    return userAdd.id;
  }

  compareUserAdd(o1: Pick<IUserAdd, 'id'> | null, o2: Pick<IUserAdd, 'id'> | null): boolean {
    return o1 && o2 ? this.getUserAddIdentifier(o1) === this.getUserAddIdentifier(o2) : o1 === o2;
  }

  addUserAddToCollectionIfMissing<Type extends Pick<IUserAdd, 'id'>>(
    userAddCollection: Type[],
    ...userAddsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const userAdds: Type[] = userAddsToCheck.filter(isPresent);
    if (userAdds.length > 0) {
      const userAddCollectionIdentifiers = userAddCollection.map(userAddItem => this.getUserAddIdentifier(userAddItem)!);
      const userAddsToAdd = userAdds.filter(userAddItem => {
        const userAddIdentifier = this.getUserAddIdentifier(userAddItem);
        if (userAddCollectionIdentifiers.includes(userAddIdentifier)) {
          return false;
        }
        userAddCollectionIdentifiers.push(userAddIdentifier);
        return true;
      });
      return [...userAddsToAdd, ...userAddCollection];
    }
    return userAddCollection;
  }
}
