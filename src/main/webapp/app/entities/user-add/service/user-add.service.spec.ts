import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IUserAdd } from '../user-add.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../user-add.test-samples';

import { UserAddService } from './user-add.service';

const requireRestSample: IUserAdd = {
  ...sampleWithRequiredData,
};

describe('UserAdd Service', () => {
  let service: UserAddService;
  let httpMock: HttpTestingController;
  let expectedResult: IUserAdd | IUserAdd[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(UserAddService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a UserAdd', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const userAdd = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(userAdd).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a UserAdd', () => {
      const userAdd = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(userAdd).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a UserAdd', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of UserAdd', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a UserAdd', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addUserAddToCollectionIfMissing', () => {
      it('should add a UserAdd to an empty array', () => {
        const userAdd: IUserAdd = sampleWithRequiredData;
        expectedResult = service.addUserAddToCollectionIfMissing([], userAdd);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(userAdd);
      });

      it('should not add a UserAdd to an array that contains it', () => {
        const userAdd: IUserAdd = sampleWithRequiredData;
        const userAddCollection: IUserAdd[] = [
          {
            ...userAdd,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addUserAddToCollectionIfMissing(userAddCollection, userAdd);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a UserAdd to an array that doesn't contain it", () => {
        const userAdd: IUserAdd = sampleWithRequiredData;
        const userAddCollection: IUserAdd[] = [sampleWithPartialData];
        expectedResult = service.addUserAddToCollectionIfMissing(userAddCollection, userAdd);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(userAdd);
      });

      it('should add only unique UserAdd to an array', () => {
        const userAddArray: IUserAdd[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const userAddCollection: IUserAdd[] = [sampleWithRequiredData];
        expectedResult = service.addUserAddToCollectionIfMissing(userAddCollection, ...userAddArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const userAdd: IUserAdd = sampleWithRequiredData;
        const userAdd2: IUserAdd = sampleWithPartialData;
        expectedResult = service.addUserAddToCollectionIfMissing([], userAdd, userAdd2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(userAdd);
        expect(expectedResult).toContain(userAdd2);
      });

      it('should accept null and undefined values', () => {
        const userAdd: IUserAdd = sampleWithRequiredData;
        expectedResult = service.addUserAddToCollectionIfMissing([], null, userAdd, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(userAdd);
      });

      it('should return initial array if no UserAdd is added', () => {
        const userAddCollection: IUserAdd[] = [sampleWithRequiredData];
        expectedResult = service.addUserAddToCollectionIfMissing(userAddCollection, undefined, null);
        expect(expectedResult).toEqual(userAddCollection);
      });
    });

    describe('compareUserAdd', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareUserAdd(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareUserAdd(entity1, entity2);
        const compareResult2 = service.compareUserAdd(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareUserAdd(entity1, entity2);
        const compareResult2 = service.compareUserAdd(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareUserAdd(entity1, entity2);
        const compareResult2 = service.compareUserAdd(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
