import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { UserAddComponent } from './list/user-add.component';
import { UserAddDetailComponent } from './detail/user-add-detail.component';
import { UserAddUpdateComponent } from './update/user-add-update.component';
import { UserAddDeleteDialogComponent } from './delete/user-add-delete-dialog.component';
import { UserAddRoutingModule } from './route/user-add-routing.module';

@NgModule({
  imports: [SharedModule, UserAddRoutingModule],
  declarations: [UserAddComponent, UserAddDetailComponent, UserAddUpdateComponent, UserAddDeleteDialogComponent],
})
export class UserAddModule {}
