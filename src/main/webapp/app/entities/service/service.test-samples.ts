import dayjs from 'dayjs/esm';

import { PeriodType } from 'app/entities/enumerations/period-type.model';
import { ChronometricType } from 'app/entities/enumerations/chronometric-type.model';

import { IService, NewService } from './service.model';

export const sampleWithRequiredData: IService = {
  id: 12311,
  name: 'Factors',
  price: 27136,
  periodType: PeriodType['ONE_TIME'],
  startDate: dayjs('2022-11-21'),
};

export const sampleWithPartialData: IService = {
  id: 11829,
  name: 'index Handmade',
  price: 97722,
  periodType: PeriodType['REGULAR'],
  chronometricType: ChronometricType['YEAR'],
  startDate: dayjs('2022-11-20'),
};

export const sampleWithFullData: IService = {
  id: 17068,
  name: 'Intelligent',
  price: 71439,
  periodType: PeriodType['ONE_TIME'],
  chronometricType: ChronometricType['MONTH'],
  periodCount: 63400,
  startDate: dayjs('2022-11-20'),
};

export const sampleWithNewData: NewService = {
  name: 'transmit Agent',
  price: 46838,
  periodType: PeriodType['ONE_TIME'],
  startDate: dayjs('2022-11-20'),
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
