import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IService, NewService } from '../service.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IService for edit and NewServiceFormGroupInput for create.
 */
type ServiceFormGroupInput = IService | PartialWithRequiredKeyOf<NewService>;

type ServiceFormDefaults = Pick<NewService, 'id' | 'groups'>;

type ServiceFormGroupContent = {
  id: FormControl<IService['id'] | NewService['id']>;
  name: FormControl<IService['name']>;
  price: FormControl<IService['price']>;
  periodType: FormControl<IService['periodType']>;
  chronometricType: FormControl<IService['chronometricType']>;
  periodCount: FormControl<IService['periodCount']>;
  startDate: FormControl<IService['startDate']>;
  groups: FormControl<IService['groups']>;
};

export type ServiceFormGroup = FormGroup<ServiceFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ServiceFormService {
  createServiceFormGroup(service: ServiceFormGroupInput = { id: null }): ServiceFormGroup {
    const serviceRawValue = {
      ...this.getFormDefaults(),
      ...service,
    };
    return new FormGroup<ServiceFormGroupContent>({
      id: new FormControl(
        { value: serviceRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      name: new FormControl(serviceRawValue.name, {
        validators: [Validators.required],
      }),
      price: new FormControl(serviceRawValue.price, {
        validators: [Validators.required],
      }),
      periodType: new FormControl(serviceRawValue.periodType, {
        validators: [Validators.required],
      }),
      chronometricType: new FormControl(serviceRawValue.chronometricType),
      periodCount: new FormControl(serviceRawValue.periodCount),
      startDate: new FormControl(serviceRawValue.startDate, {
        validators: [Validators.required],
      }),
      groups: new FormControl(serviceRawValue.groups ?? []),
    });
  }

  getService(form: ServiceFormGroup): IService | NewService {
    return form.getRawValue() as IService | NewService;
  }

  resetForm(form: ServiceFormGroup, service: ServiceFormGroupInput): void {
    const serviceRawValue = { ...this.getFormDefaults(), ...service };
    form.reset(
      {
        ...serviceRawValue,
        id: { value: serviceRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): ServiceFormDefaults {
    return {
      id: null,
      groups: [],
    };
  }
}
