import dayjs from 'dayjs/esm';
import { IGroups } from 'app/entities/groups/groups.model';
import { PeriodType } from 'app/entities/enumerations/period-type.model';
import { ChronometricType } from 'app/entities/enumerations/chronometric-type.model';

export interface IService {
  id: number;
  name?: string | null;
  price?: number | null;
  periodType?: PeriodType | null;
  chronometricType?: ChronometricType | null;
  periodCount?: number | null;
  startDate?: dayjs.Dayjs | null;
  groups?: Pick<IGroups, 'id'>[] | null;
}

export type NewService = Omit<IService, 'id'> & { id: null };
