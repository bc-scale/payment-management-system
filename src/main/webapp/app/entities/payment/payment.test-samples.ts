import dayjs from 'dayjs/esm';

import { IPayment, NewPayment } from './payment.model';

export const sampleWithRequiredData: IPayment = {
  id: 47537,
  paymentFrom: dayjs('2022-11-20'),
  paymentTo: dayjs('2022-11-20'),
  price: 47604,
  payedMoney: 34993,
};

export const sampleWithPartialData: IPayment = {
  id: 88670,
  paymentFrom: dayjs('2022-11-20'),
  paymentTo: dayjs('2022-11-20'),
  price: 28713,
  payedMoney: 32065,
};

export const sampleWithFullData: IPayment = {
  id: 53029,
  paymentFrom: dayjs('2022-11-20'),
  paymentTo: dayjs('2022-11-20'),
  price: 89152,
  payedMoney: 90810,
};

export const sampleWithNewData: NewPayment = {
  paymentFrom: dayjs('2022-11-20'),
  paymentTo: dayjs('2022-11-20'),
  price: 77766,
  payedMoney: 12909,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
