import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IPayment, NewPayment } from '../payment.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IPayment for edit and NewPaymentFormGroupInput for create.
 */
type PaymentFormGroupInput = IPayment | PartialWithRequiredKeyOf<NewPayment>;

type PaymentFormDefaults = Pick<NewPayment, 'id'>;

type PaymentFormGroupContent = {
  id: FormControl<IPayment['id'] | NewPayment['id']>;
  paymentFrom: FormControl<IPayment['paymentFrom']>;
  paymentTo: FormControl<IPayment['paymentTo']>;
  price: FormControl<IPayment['price']>;
  payedMoney: FormControl<IPayment['payedMoney']>;
  payer: FormControl<IPayment['payer']>;
  service: FormControl<IPayment['service']>;
  group: FormControl<IPayment['group']>;
};

export type PaymentFormGroup = FormGroup<PaymentFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class PaymentFormService {
  createPaymentFormGroup(payment: PaymentFormGroupInput = { id: null }): PaymentFormGroup {
    const paymentRawValue = {
      ...this.getFormDefaults(),
      ...payment,
    };
    return new FormGroup<PaymentFormGroupContent>({
      id: new FormControl(
        { value: paymentRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      paymentFrom: new FormControl(paymentRawValue.paymentFrom, {
        validators: [Validators.required],
      }),
      paymentTo: new FormControl(paymentRawValue.paymentTo, {
        validators: [Validators.required],
      }),
      price: new FormControl(paymentRawValue.price, {
        validators: [Validators.required],
      }),
      payedMoney: new FormControl(paymentRawValue.payedMoney, {
        validators: [Validators.required],
      }),
      payer: new FormControl(paymentRawValue.payer),
      service: new FormControl(paymentRawValue.service),
      group: new FormControl(paymentRawValue.group),
    });
  }

  getPayment(form: PaymentFormGroup): IPayment | NewPayment {
    return form.getRawValue() as IPayment | NewPayment;
  }

  resetForm(form: PaymentFormGroup, payment: PaymentFormGroupInput): void {
    const paymentRawValue = { ...this.getFormDefaults(), ...payment };
    form.reset(
      {
        ...paymentRawValue,
        id: { value: paymentRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): PaymentFormDefaults {
    return {
      id: null,
    };
  }
}
