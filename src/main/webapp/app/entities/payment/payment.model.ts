import dayjs from 'dayjs/esm';
import { IUser } from 'app/entities/user/user.model';
import { IService } from 'app/entities/service/service.model';
import { IGroups } from 'app/entities/groups/groups.model';

export interface IPayment {
  id: number;
  paymentFrom?: dayjs.Dayjs | null;
  paymentTo?: dayjs.Dayjs | null;
  price?: number | null;
  payedMoney?: number | null;
  payer?: Pick<IUser, 'id'> | null;
  service?: Pick<IService, 'id'> | null;
  group?: Pick<IGroups, 'id'> | null;
}

export type NewPayment = Omit<IPayment, 'id'> & { id: null };
