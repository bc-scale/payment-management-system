import { IUser } from 'app/entities/user/user.model';

export interface IOrganization {
  id: number;
  name?: string | null;
  account?: number | null;
  owner?: Pick<IUser, 'id'> | null;
}

export type NewOrganization = Omit<IOrganization, 'id'> & { id: null };
