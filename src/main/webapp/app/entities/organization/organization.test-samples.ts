import { IOrganization, NewOrganization } from './organization.model';

export const sampleWithRequiredData: IOrganization = {
  id: 12784,
  name: 'Automated Rial',
  account: 53520,
};

export const sampleWithPartialData: IOrganization = {
  id: 57434,
  name: 'Utah',
  account: 27486,
};

export const sampleWithFullData: IOrganization = {
  id: 85104,
  name: 'Fantastic systems',
  account: 76426,
};

export const sampleWithNewData: NewOrganization = {
  name: 'disintermediate quantifying',
  account: 79503,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
