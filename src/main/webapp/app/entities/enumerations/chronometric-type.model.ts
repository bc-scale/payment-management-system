export enum ChronometricType {
  DAY = 'DAY',

  WEAK = 'WEAK',

  MONTH = 'MONTH',

  YEAR = 'YEAR',
}
