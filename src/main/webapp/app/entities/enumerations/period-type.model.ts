export enum PeriodType {
  ONE_TIME = 'ONE_TIME',

  REGULAR = 'REGULAR',
}
