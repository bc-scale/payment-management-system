import { IUser } from 'app/entities/user/user.model';
import { IOrganization } from 'app/entities/organization/organization.model';
import { IUserAdd } from 'app/entities/user-add/user-add.model';
import { IService } from 'app/entities/service/service.model';

export interface IGroups {
  id: number;
  name?: string | null;
  manager?: Pick<IUser, 'id'> | null;
  organization?: Pick<IOrganization, 'id'> | null;
  users?: Pick<IUserAdd, 'id'>[] | null;
  services?: Pick<IService, 'id'>[] | null;
}

export type NewGroups = Omit<IGroups, 'id'> & { id: null };
