import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { GroupsFormService } from './groups-form.service';
import { GroupsService } from '../service/groups.service';
import { IGroups } from '../groups.model';

import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { IOrganization } from 'app/entities/organization/organization.model';
import { OrganizationService } from 'app/entities/organization/service/organization.service';
import { IUserAdd } from 'app/entities/user-add/user-add.model';
import { UserAddService } from 'app/entities/user-add/service/user-add.service';
import { IService } from 'app/entities/service/service.model';
import { ServiceService } from 'app/entities/service/service/service.service';

import { GroupsUpdateComponent } from './groups-update.component';

describe('Groups Management Update Component', () => {
  let comp: GroupsUpdateComponent;
  let fixture: ComponentFixture<GroupsUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let groupsFormService: GroupsFormService;
  let groupsService: GroupsService;
  let userService: UserService;
  let organizationService: OrganizationService;
  let userAddService: UserAddService;
  let serviceService: ServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [GroupsUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(GroupsUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(GroupsUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    groupsFormService = TestBed.inject(GroupsFormService);
    groupsService = TestBed.inject(GroupsService);
    userService = TestBed.inject(UserService);
    organizationService = TestBed.inject(OrganizationService);
    userAddService = TestBed.inject(UserAddService);
    serviceService = TestBed.inject(ServiceService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call User query and add missing value', () => {
      const groups: IGroups = { id: 456 };
      const manager: IUser = { id: 71813 };
      groups.manager = manager;

      const userCollection: IUser[] = [{ id: 52684 }];
      jest.spyOn(userService, 'query').mockReturnValue(of(new HttpResponse({ body: userCollection })));
      const additionalUsers = [manager];
      const expectedCollection: IUser[] = [...additionalUsers, ...userCollection];
      jest.spyOn(userService, 'addUserToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ groups });
      comp.ngOnInit();

      expect(userService.query).toHaveBeenCalled();
      expect(userService.addUserToCollectionIfMissing).toHaveBeenCalledWith(
        userCollection,
        ...additionalUsers.map(expect.objectContaining)
      );
      expect(comp.usersSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Organization query and add missing value', () => {
      const groups: IGroups = { id: 456 };
      const organization: IOrganization = { id: 50891 };
      groups.organization = organization;

      const organizationCollection: IOrganization[] = [{ id: 69494 }];
      jest.spyOn(organizationService, 'query').mockReturnValue(of(new HttpResponse({ body: organizationCollection })));
      const additionalOrganizations = [organization];
      const expectedCollection: IOrganization[] = [...additionalOrganizations, ...organizationCollection];
      jest.spyOn(organizationService, 'addOrganizationToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ groups });
      comp.ngOnInit();

      expect(organizationService.query).toHaveBeenCalled();
      expect(organizationService.addOrganizationToCollectionIfMissing).toHaveBeenCalledWith(
        organizationCollection,
        ...additionalOrganizations.map(expect.objectContaining)
      );
      expect(comp.organizationsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call UserAdd query and add missing value', () => {
      const groups: IGroups = { id: 456 };
      const users: IUserAdd[] = [{ id: 73519 }];
      groups.users = users;

      const userAddCollection: IUserAdd[] = [{ id: 1696 }];
      jest.spyOn(userAddService, 'query').mockReturnValue(of(new HttpResponse({ body: userAddCollection })));
      const additionalUserAdds = [...users];
      const expectedCollection: IUserAdd[] = [...additionalUserAdds, ...userAddCollection];
      jest.spyOn(userAddService, 'addUserAddToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ groups });
      comp.ngOnInit();

      expect(userAddService.query).toHaveBeenCalled();
      expect(userAddService.addUserAddToCollectionIfMissing).toHaveBeenCalledWith(
        userAddCollection,
        ...additionalUserAdds.map(expect.objectContaining)
      );
      expect(comp.userAddsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Service query and add missing value', () => {
      const groups: IGroups = { id: 456 };
      const services: IService[] = [{ id: 96719 }];
      groups.services = services;

      const serviceCollection: IService[] = [{ id: 63823 }];
      jest.spyOn(serviceService, 'query').mockReturnValue(of(new HttpResponse({ body: serviceCollection })));
      const additionalServices = [...services];
      const expectedCollection: IService[] = [...additionalServices, ...serviceCollection];
      jest.spyOn(serviceService, 'addServiceToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ groups });
      comp.ngOnInit();

      expect(serviceService.query).toHaveBeenCalled();
      expect(serviceService.addServiceToCollectionIfMissing).toHaveBeenCalledWith(
        serviceCollection,
        ...additionalServices.map(expect.objectContaining)
      );
      expect(comp.servicesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const groups: IGroups = { id: 456 };
      const manager: IUser = { id: 50661 };
      groups.manager = manager;
      const organization: IOrganization = { id: 87226 };
      groups.organization = organization;
      const users: IUserAdd = { id: 46377 };
      groups.users = [users];
      const services: IService = { id: 60106 };
      groups.services = [services];

      activatedRoute.data = of({ groups });
      comp.ngOnInit();

      expect(comp.usersSharedCollection).toContain(manager);
      expect(comp.organizationsSharedCollection).toContain(organization);
      expect(comp.userAddsSharedCollection).toContain(users);
      expect(comp.servicesSharedCollection).toContain(services);
      expect(comp.groups).toEqual(groups);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IGroups>>();
      const groups = { id: 123 };
      jest.spyOn(groupsFormService, 'getGroups').mockReturnValue(groups);
      jest.spyOn(groupsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ groups });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: groups }));
      saveSubject.complete();

      // THEN
      expect(groupsFormService.getGroups).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(groupsService.update).toHaveBeenCalledWith(expect.objectContaining(groups));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IGroups>>();
      const groups = { id: 123 };
      jest.spyOn(groupsFormService, 'getGroups').mockReturnValue({ id: null });
      jest.spyOn(groupsService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ groups: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: groups }));
      saveSubject.complete();

      // THEN
      expect(groupsFormService.getGroups).toHaveBeenCalled();
      expect(groupsService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IGroups>>();
      const groups = { id: 123 };
      jest.spyOn(groupsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ groups });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(groupsService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareUser', () => {
      it('Should forward to userService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(userService, 'compareUser');
        comp.compareUser(entity, entity2);
        expect(userService.compareUser).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareOrganization', () => {
      it('Should forward to organizationService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(organizationService, 'compareOrganization');
        comp.compareOrganization(entity, entity2);
        expect(organizationService.compareOrganization).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareUserAdd', () => {
      it('Should forward to userAddService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(userAddService, 'compareUserAdd');
        comp.compareUserAdd(entity, entity2);
        expect(userAddService.compareUserAdd).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareService', () => {
      it('Should forward to serviceService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(serviceService, 'compareService');
        comp.compareService(entity, entity2);
        expect(serviceService.compareService).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
