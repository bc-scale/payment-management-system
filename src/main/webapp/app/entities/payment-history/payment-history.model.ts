import dayjs from 'dayjs/esm';

export interface IPaymentHistory {
  id: number;
  organizationName?: string | null;
  serviceName?: string | null;
  groupsName?: string | null;
  userId?: number | null;
  paymentFrom?: dayjs.Dayjs | null;
  paymentTo?: dayjs.Dayjs | null;
  createdAt?: dayjs.Dayjs | null;
}

export type NewPaymentHistory = Omit<IPaymentHistory, 'id'> & { id: null };
