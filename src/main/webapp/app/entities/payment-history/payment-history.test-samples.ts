import dayjs from 'dayjs/esm';

import { IPaymentHistory, NewPaymentHistory } from './payment-history.model';

export const sampleWithRequiredData: IPaymentHistory = {
  id: 53333,
  organizationName: 'Union Soft Steel',
  serviceName: 'Lev Cloned invoice',
  groupsName: 'synergy',
  userId: 50474,
  paymentFrom: dayjs('2022-11-20'),
  paymentTo: dayjs('2022-11-21'),
  createdAt: dayjs('2022-11-21'),
};

export const sampleWithPartialData: IPaymentHistory = {
  id: 38160,
  organizationName: 'Configurable Buckinghamshire',
  serviceName: 'RSS',
  groupsName: 'program',
  userId: 93097,
  paymentFrom: dayjs('2022-11-20'),
  paymentTo: dayjs('2022-11-20'),
  createdAt: dayjs('2022-11-21'),
};

export const sampleWithFullData: IPaymentHistory = {
  id: 73271,
  organizationName: 'Principal',
  serviceName: 'Cambridgeshire',
  groupsName: 'Fresh',
  userId: 44082,
  paymentFrom: dayjs('2022-11-20'),
  paymentTo: dayjs('2022-11-20'),
  createdAt: dayjs('2022-11-20'),
};

export const sampleWithNewData: NewPaymentHistory = {
  organizationName: '24/365 mobile cross-platform',
  serviceName: 'feed Facilitator',
  groupsName: 'bypass',
  userId: 29770,
  paymentFrom: dayjs('2022-11-21'),
  paymentTo: dayjs('2022-11-20'),
  createdAt: dayjs('2022-11-20'),
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
