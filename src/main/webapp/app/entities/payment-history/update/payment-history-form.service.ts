import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IPaymentHistory, NewPaymentHistory } from '../payment-history.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IPaymentHistory for edit and NewPaymentHistoryFormGroupInput for create.
 */
type PaymentHistoryFormGroupInput = IPaymentHistory | PartialWithRequiredKeyOf<NewPaymentHistory>;

type PaymentHistoryFormDefaults = Pick<NewPaymentHistory, 'id'>;

type PaymentHistoryFormGroupContent = {
  id: FormControl<IPaymentHistory['id'] | NewPaymentHistory['id']>;
  organizationName: FormControl<IPaymentHistory['organizationName']>;
  serviceName: FormControl<IPaymentHistory['serviceName']>;
  groupsName: FormControl<IPaymentHistory['groupsName']>;
  userId: FormControl<IPaymentHistory['userId']>;
  paymentFrom: FormControl<IPaymentHistory['paymentFrom']>;
  paymentTo: FormControl<IPaymentHistory['paymentTo']>;
  createdAt: FormControl<IPaymentHistory['createdAt']>;
};

export type PaymentHistoryFormGroup = FormGroup<PaymentHistoryFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class PaymentHistoryFormService {
  createPaymentHistoryFormGroup(paymentHistory: PaymentHistoryFormGroupInput = { id: null }): PaymentHistoryFormGroup {
    const paymentHistoryRawValue = {
      ...this.getFormDefaults(),
      ...paymentHistory,
    };
    return new FormGroup<PaymentHistoryFormGroupContent>({
      id: new FormControl(
        { value: paymentHistoryRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      organizationName: new FormControl(paymentHistoryRawValue.organizationName, {
        validators: [Validators.required],
      }),
      serviceName: new FormControl(paymentHistoryRawValue.serviceName, {
        validators: [Validators.required],
      }),
      groupsName: new FormControl(paymentHistoryRawValue.groupsName, {
        validators: [Validators.required],
      }),
      userId: new FormControl(paymentHistoryRawValue.userId, {
        validators: [Validators.required],
      }),
      paymentFrom: new FormControl(paymentHistoryRawValue.paymentFrom, {
        validators: [Validators.required],
      }),
      paymentTo: new FormControl(paymentHistoryRawValue.paymentTo, {
        validators: [Validators.required],
      }),
      createdAt: new FormControl(paymentHistoryRawValue.createdAt, {
        validators: [Validators.required],
      }),
    });
  }

  getPaymentHistory(form: PaymentHistoryFormGroup): IPaymentHistory | NewPaymentHistory {
    return form.getRawValue() as IPaymentHistory | NewPaymentHistory;
  }

  resetForm(form: PaymentHistoryFormGroup, paymentHistory: PaymentHistoryFormGroupInput): void {
    const paymentHistoryRawValue = { ...this.getFormDefaults(), ...paymentHistory };
    form.reset(
      {
        ...paymentHistoryRawValue,
        id: { value: paymentHistoryRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): PaymentHistoryFormDefaults {
    return {
      id: null,
    };
  }
}
