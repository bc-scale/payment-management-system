package uz.devops.pms.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import uz.devops.pms.repository.UserAddRepository;
import uz.devops.pms.service.UserAddService;
import uz.devops.pms.service.dto.UserAddDTO;
import uz.devops.pms.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link uz.devops.pms.domain.UserAdd}.
 */
@RestController
@RequestMapping("/api")
public class UserAddResource {

    private final Logger log = LoggerFactory.getLogger(UserAddResource.class);

    private static final String ENTITY_NAME = "userAdd";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserAddService userAddService;

    private final UserAddRepository userAddRepository;

    public UserAddResource(UserAddService userAddService, UserAddRepository userAddRepository) {
        this.userAddService = userAddService;
        this.userAddRepository = userAddRepository;
    }

    /**
     * {@code POST  /user-adds} : Create a new userAdd.
     *
     * @param userAddDTO the userAddDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userAddDTO, or with status {@code 400 (Bad Request)} if the userAdd has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-adds")
    public ResponseEntity<UserAddDTO> createUserAdd(@RequestBody UserAddDTO userAddDTO) throws URISyntaxException {
        log.debug("REST request to save UserAdd : {}", userAddDTO);
        if (userAddDTO.getId() != null) {
            throw new BadRequestAlertException("A new userAdd cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserAddDTO result = userAddService.save(userAddDTO);
        return ResponseEntity
            .created(new URI("/api/user-adds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-adds/:id} : Updates an existing userAdd.
     *
     * @param id the id of the userAddDTO to save.
     * @param userAddDTO the userAddDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userAddDTO,
     * or with status {@code 400 (Bad Request)} if the userAddDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userAddDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-adds/{id}")
    public ResponseEntity<UserAddDTO> updateUserAdd(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody UserAddDTO userAddDTO
    ) throws URISyntaxException {
        log.debug("REST request to update UserAdd : {}, {}", id, userAddDTO);
        if (userAddDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userAddDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userAddRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        UserAddDTO result = userAddService.update(userAddDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userAddDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /user-adds/:id} : Partial updates given fields of an existing userAdd, field will ignore if it is null
     *
     * @param id the id of the userAddDTO to save.
     * @param userAddDTO the userAddDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userAddDTO,
     * or with status {@code 400 (Bad Request)} if the userAddDTO is not valid,
     * or with status {@code 404 (Not Found)} if the userAddDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the userAddDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/user-adds/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<UserAddDTO> partialUpdateUserAdd(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody UserAddDTO userAddDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update UserAdd partially : {}, {}", id, userAddDTO);
        if (userAddDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userAddDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userAddRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<UserAddDTO> result = userAddService.partialUpdate(userAddDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userAddDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /user-adds} : get all the userAdds.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userAdds in body.
     */
    @GetMapping("/user-adds")
    public List<UserAddDTO> getAllUserAdds() {
        log.debug("REST request to get all UserAdds");
        return userAddService.findAll();
    }

    /**
     * {@code GET  /user-adds/:id} : get the "id" userAdd.
     *
     * @param id the id of the userAddDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userAddDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-adds/{id}")
    public ResponseEntity<UserAddDTO> getUserAdd(@PathVariable Long id) {
        log.debug("REST request to get UserAdd : {}", id);
        Optional<UserAddDTO> userAddDTO = userAddService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userAddDTO);
    }

    /**
     * {@code DELETE  /user-adds/:id} : delete the "id" userAdd.
     *
     * @param id the id of the userAddDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-adds/{id}")
    public ResponseEntity<Void> deleteUserAdd(@PathVariable Long id) {
        log.debug("REST request to delete UserAdd : {}", id);
        userAddService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
