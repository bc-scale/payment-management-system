package uz.devops.pms.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A Payment.
 */
@Entity
@Table(name = "payment")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Payment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "payment_from", nullable = false)
    private LocalDate paymentFrom;

    @NotNull
    @Column(name = "payment_to", nullable = false)
    private LocalDate paymentTo;

    @NotNull
    @Column(name = "price", nullable = false)
    private Double price;

    @NotNull
    @Column(name = "payed_money", nullable = false)
    private Double payedMoney;

    @ManyToOne
    private User payer;

    @ManyToOne
    @JsonIgnoreProperties(value = { "groups" }, allowSetters = true)
    private Service service;

    @ManyToOne
    @JsonIgnoreProperties(value = { "manager", "organization", "users", "services" }, allowSetters = true)
    private Groups group;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Payment id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getPaymentFrom() {
        return this.paymentFrom;
    }

    public Payment paymentFrom(LocalDate paymentFrom) {
        this.setPaymentFrom(paymentFrom);
        return this;
    }

    public void setPaymentFrom(LocalDate paymentFrom) {
        this.paymentFrom = paymentFrom;
    }

    public LocalDate getPaymentTo() {
        return this.paymentTo;
    }

    public Payment paymentTo(LocalDate paymentTo) {
        this.setPaymentTo(paymentTo);
        return this;
    }

    public void setPaymentTo(LocalDate paymentTo) {
        this.paymentTo = paymentTo;
    }

    public Double getPrice() {
        return this.price;
    }

    public Payment price(Double price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPayedMoney() {
        return this.payedMoney;
    }

    public Payment payedMoney(Double payedMoney) {
        this.setPayedMoney(payedMoney);
        return this;
    }

    public void setPayedMoney(Double payedMoney) {
        this.payedMoney = payedMoney;
    }

    public User getPayer() {
        return this.payer;
    }

    public void setPayer(User user) {
        this.payer = user;
    }

    public Payment payer(User user) {
        this.setPayer(user);
        return this;
    }

    public Service getService() {
        return this.service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Payment service(Service service) {
        this.setService(service);
        return this;
    }

    public Groups getGroup() {
        return this.group;
    }

    public void setGroup(Groups groups) {
        this.group = groups;
    }

    public Payment group(Groups groups) {
        this.setGroup(groups);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Payment)) {
            return false;
        }
        return id != null && id.equals(((Payment) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Payment{" +
            "id=" + getId() +
            ", paymentFrom='" + getPaymentFrom() + "'" +
            ", paymentTo='" + getPaymentTo() + "'" +
            ", price=" + getPrice() +
            ", payedMoney=" + getPayedMoney() +
            "}";
    }
}
