package uz.devops.pms.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A PaymentHistory.
 */
@Entity
@Table(name = "payment_history")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PaymentHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "organization_name", nullable = false)
    private String organizationName;

    @NotNull
    @Column(name = "service_name", nullable = false)
    private String serviceName;

    @NotNull
    @Column(name = "groups_name", nullable = false)
    private String groupsName;

    @NotNull
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @NotNull
    @Column(name = "payment_from", nullable = false)
    private LocalDate paymentFrom;

    @NotNull
    @Column(name = "payment_to", nullable = false)
    private LocalDate paymentTo;

    @NotNull
    @Column(name = "created_at", nullable = false)
    private LocalDate createdAt;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PaymentHistory id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganizationName() {
        return this.organizationName;
    }

    public PaymentHistory organizationName(String organizationName) {
        this.setOrganizationName(organizationName);
        return this;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getServiceName() {
        return this.serviceName;
    }

    public PaymentHistory serviceName(String serviceName) {
        this.setServiceName(serviceName);
        return this;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getGroupsName() {
        return this.groupsName;
    }

    public PaymentHistory groupsName(String groupsName) {
        this.setGroupsName(groupsName);
        return this;
    }

    public void setGroupsName(String groupsName) {
        this.groupsName = groupsName;
    }

    public Long getUserId() {
        return this.userId;
    }

    public PaymentHistory userId(Long userId) {
        this.setUserId(userId);
        return this;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDate getPaymentFrom() {
        return this.paymentFrom;
    }

    public PaymentHistory paymentFrom(LocalDate paymentFrom) {
        this.setPaymentFrom(paymentFrom);
        return this;
    }

    public void setPaymentFrom(LocalDate paymentFrom) {
        this.paymentFrom = paymentFrom;
    }

    public LocalDate getPaymentTo() {
        return this.paymentTo;
    }

    public PaymentHistory paymentTo(LocalDate paymentTo) {
        this.setPaymentTo(paymentTo);
        return this;
    }

    public void setPaymentTo(LocalDate paymentTo) {
        this.paymentTo = paymentTo;
    }

    public LocalDate getCreatedAt() {
        return this.createdAt;
    }

    public PaymentHistory createdAt(LocalDate createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaymentHistory)) {
            return false;
        }
        return id != null && id.equals(((PaymentHistory) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaymentHistory{" +
            "id=" + getId() +
            ", organizationName='" + getOrganizationName() + "'" +
            ", serviceName='" + getServiceName() + "'" +
            ", groupsName='" + getGroupsName() + "'" +
            ", userId=" + getUserId() +
            ", paymentFrom='" + getPaymentFrom() + "'" +
            ", paymentTo='" + getPaymentTo() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            "}";
    }
}
