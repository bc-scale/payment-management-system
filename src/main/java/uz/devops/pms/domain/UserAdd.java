package uz.devops.pms.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A UserAdd.
 */
@Entity
@Table(name = "user_add")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UserAdd implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "account")
    private Double account;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @ManyToMany(mappedBy = "users")
    @JsonIgnoreProperties(value = { "manager", "organization", "users", "services" }, allowSetters = true)
    private Set<Groups> groups = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public UserAdd id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAccount() {
        return this.account;
    }

    public UserAdd account(Double account) {
        this.setAccount(account);
        return this;
    }

    public void setAccount(Double account) {
        this.account = account;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserAdd user(User user) {
        this.setUser(user);
        return this;
    }

    public Set<Groups> getGroups() {
        return this.groups;
    }

    public void setGroups(Set<Groups> groups) {
        if (this.groups != null) {
            this.groups.forEach(i -> i.removeUsers(this));
        }
        if (groups != null) {
            groups.forEach(i -> i.addUsers(this));
        }
        this.groups = groups;
    }

    public UserAdd groups(Set<Groups> groups) {
        this.setGroups(groups);
        return this;
    }

    public UserAdd addGroups(Groups groups) {
        this.groups.add(groups);
        groups.getUsers().add(this);
        return this;
    }

    public UserAdd removeGroups(Groups groups) {
        this.groups.remove(groups);
        groups.getUsers().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserAdd)) {
            return false;
        }
        return id != null && id.equals(((UserAdd) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserAdd{" +
            "id=" + getId() +
            ", account=" + getAccount() +
            "}";
    }
}
