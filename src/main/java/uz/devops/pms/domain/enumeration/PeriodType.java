package uz.devops.pms.domain.enumeration;

/**
 * The PeriodType enumeration.
 */
public enum PeriodType {
    ONE_TIME,
    REGULAR,
}
