package uz.devops.pms.domain.enumeration;

/**
 * The ChronometricType enumeration.
 */
public enum ChronometricType {
    DAY,
    WEAK,
    MONTH,
    YEAR,
}
