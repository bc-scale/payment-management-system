package uz.devops.pms.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A Groups.
 */
@Entity
@Table(name = "groups")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Groups implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne
    private User manager;

    @ManyToOne
    @JsonIgnoreProperties(value = { "owner", "groups" }, allowSetters = true)
    private Organization organization;

    @ManyToMany
    @JoinTable(
        name = "rel_groups__users",
        joinColumns = @JoinColumn(name = "groups_id"),
        inverseJoinColumns = @JoinColumn(name = "users_id")
    )
    @JsonIgnoreProperties(value = { "user", "groups" }, allowSetters = true)
    private Set<UserAdd> users = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_groups__services",
        joinColumns = @JoinColumn(name = "groups_id"),
        inverseJoinColumns = @JoinColumn(name = "services_id")
    )
    @JsonIgnoreProperties(value = { "groups" }, allowSetters = true)
    private Set<Service> services = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Groups id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Groups name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getManager() {
        return this.manager;
    }

    public void setManager(User user) {
        this.manager = user;
    }

    public Groups manager(User user) {
        this.setManager(user);
        return this;
    }

    public Organization getOrganization() {
        return this.organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Groups organization(Organization organization) {
        this.setOrganization(organization);
        return this;
    }

    public Set<UserAdd> getUsers() {
        return this.users;
    }

    public void setUsers(Set<UserAdd> userAdds) {
        this.users = userAdds;
    }

    public Groups users(Set<UserAdd> userAdds) {
        this.setUsers(userAdds);
        return this;
    }

    public Groups addUsers(UserAdd userAdd) {
        this.users.add(userAdd);
        userAdd.getGroups().add(this);
        return this;
    }

    public Groups removeUsers(UserAdd userAdd) {
        this.users.remove(userAdd);
        userAdd.getGroups().remove(this);
        return this;
    }

    public Set<Service> getServices() {
        return this.services;
    }

    public void setServices(Set<Service> services) {
        this.services = services;
    }

    public Groups services(Set<Service> services) {
        this.setServices(services);
        return this;
    }

    public Groups addServices(Service service) {
        this.services.add(service);
        service.getGroups().add(this);
        return this;
    }

    public Groups removeServices(Service service) {
        this.services.remove(service);
        service.getGroups().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Groups)) {
            return false;
        }
        return id != null && id.equals(((Groups) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Groups{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
