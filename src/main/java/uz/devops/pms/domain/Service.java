package uz.devops.pms.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import uz.devops.pms.domain.enumeration.ChronometricType;
import uz.devops.pms.domain.enumeration.PeriodType;

/**
 * A Service.
 */
@Entity
@Table(name = "service")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Service implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "price", nullable = false)
    private Double price;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "period_type", nullable = false)
    private PeriodType periodType;

    @Enumerated(EnumType.STRING)
    @Column(name = "chronometric_type")
    private ChronometricType chronometricType;

    @Column(name = "period_count")
    private Integer periodCount;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @ManyToMany(mappedBy = "services")
    @JsonIgnoreProperties(value = { "manager", "organization", "users", "services" }, allowSetters = true)
    private Set<Groups> groups = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Service id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Service name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return this.price;
    }

    public Service price(Double price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public PeriodType getPeriodType() {
        return this.periodType;
    }

    public Service periodType(PeriodType periodType) {
        this.setPeriodType(periodType);
        return this;
    }

    public void setPeriodType(PeriodType periodType) {
        this.periodType = periodType;
    }

    public ChronometricType getChronometricType() {
        return this.chronometricType;
    }

    public Service chronometricType(ChronometricType chronometricType) {
        this.setChronometricType(chronometricType);
        return this;
    }

    public void setChronometricType(ChronometricType chronometricType) {
        this.chronometricType = chronometricType;
    }

    public Integer getPeriodCount() {
        return this.periodCount;
    }

    public Service periodCount(Integer periodCount) {
        this.setPeriodCount(periodCount);
        return this;
    }

    public void setPeriodCount(Integer periodCount) {
        this.periodCount = periodCount;
    }

    public LocalDate getStartDate() {
        return this.startDate;
    }

    public Service startDate(LocalDate startDate) {
        this.setStartDate(startDate);
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public Set<Groups> getGroups() {
        return this.groups;
    }

    public void setGroups(Set<Groups> groups) {
        if (this.groups != null) {
            this.groups.forEach(i -> i.removeServices(this));
        }
        if (groups != null) {
            groups.forEach(i -> i.addServices(this));
        }
        this.groups = groups;
    }

    public Service groups(Set<Groups> groups) {
        this.setGroups(groups);
        return this;
    }

    public Service addGroups(Groups groups) {
        this.groups.add(groups);
        groups.getServices().add(this);
        return this;
    }

    public Service removeGroups(Groups groups) {
        this.groups.remove(groups);
        groups.getServices().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Service)) {
            return false;
        }
        return id != null && id.equals(((Service) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Service{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", price=" + getPrice() +
            ", periodType='" + getPeriodType() + "'" +
            ", chronometricType='" + getChronometricType() + "'" +
            ", periodCount=" + getPeriodCount() +
            ", startDate='" + getStartDate() + "'" +
            "}";
    }
}
