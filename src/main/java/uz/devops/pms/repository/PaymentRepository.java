package uz.devops.pms.repository;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.devops.pms.domain.Payment;

/**
 * Spring Data JPA repository for the Payment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
    @Query("select payment from Payment payment where payment.payer.login = ?#{principal.username}")
    List<Payment> findByPayerIsCurrentUser();

    Payment findFirstByPayerIdAndServiceIdOrderByPaymentToDesc(Long payer_id, Long service_id);

    List<Payment> findAllByPayerIdAndServiceId(Long payer_id, Long service_id);
}
