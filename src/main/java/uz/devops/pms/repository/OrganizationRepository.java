package uz.devops.pms.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import uz.devops.pms.domain.Organization;

/**
 * Spring Data JPA repository for the Organization entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Long> {
    @Query("select organization from Organization organization where organization.owner.login = ?#{principal.username}")
    List<Organization> findByOwnerIsCurrentUser();

    Optional<Organization> findByName(String name);
}
