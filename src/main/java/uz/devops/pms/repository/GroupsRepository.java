package uz.devops.pms.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.devops.pms.domain.Groups;

/**
 * Spring Data JPA repository for the Groups entity.
 *
 * When extending this class, extend GroupsRepositoryWithBagRelationships too.
 * For more information refer to https://github.com/jhipster/generator-jhipster/issues/17990.
 */
@Repository
public interface GroupsRepository extends GroupsRepositoryWithBagRelationships, JpaRepository<Groups, Long> {
    @Query("select groups from Groups groups where groups.manager.login = ?#{principal.username}")
    List<Groups> findByManagerIsCurrentUser();

    default Optional<Groups> findOneWithEagerRelationships(Long id) {
        return this.fetchBagRelationships(this.findById(id));
    }

    default List<Groups> findAllWithEagerRelationships() {
        return this.fetchBagRelationships(this.findAll());
    }

    default Page<Groups> findAllWithEagerRelationships(Pageable pageable) {
        return this.fetchBagRelationships(this.findAll(pageable));
    }
}
