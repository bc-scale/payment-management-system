package uz.devops.pms.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import uz.devops.pms.domain.UserAdd;

/**
 * Spring Data JPA repository for the UserAdd entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserAddRepository extends JpaRepository<UserAdd, Long> {
    UserAdd findByUserId(Long userId);
}
