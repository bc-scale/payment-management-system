package uz.devops.pms.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import uz.devops.pms.domain.Authority;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
    Optional<Authority> findByName(String name);
}
