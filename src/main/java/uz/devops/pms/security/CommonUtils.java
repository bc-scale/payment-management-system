package uz.devops.pms.security;

import java.util.Optional;
import org.springframework.stereotype.Component;
import uz.devops.pms.domain.User;
import uz.devops.pms.repository.UserRepository;

@Component
public class CommonUtils {

    private final UserRepository userRepository;

    public CommonUtils(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> getUser() {
        return userRepository.findOneByLogin(
            SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new IllegalStateException("User not found"))
        );
    }
}
