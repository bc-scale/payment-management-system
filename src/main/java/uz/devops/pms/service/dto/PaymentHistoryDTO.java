package uz.devops.pms.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link uz.devops.pms.domain.PaymentHistory} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PaymentHistoryDTO implements Serializable {

    private Long id;

    @NotNull
    private String organizationName;

    @NotNull
    private String serviceName;

    @NotNull
    private String groupsName;

    @NotNull
    private Long userId;

    @NotNull
    private LocalDate paymentFrom;

    @NotNull
    private LocalDate paymentTo;

    @NotNull
    private LocalDate createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getGroupsName() {
        return groupsName;
    }

    public void setGroupsName(String groupsName) {
        this.groupsName = groupsName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDate getPaymentFrom() {
        return paymentFrom;
    }

    public void setPaymentFrom(LocalDate paymentFrom) {
        this.paymentFrom = paymentFrom;
    }

    public LocalDate getPaymentTo() {
        return paymentTo;
    }

    public void setPaymentTo(LocalDate paymentTo) {
        this.paymentTo = paymentTo;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaymentHistoryDTO)) {
            return false;
        }

        PaymentHistoryDTO paymentHistoryDTO = (PaymentHistoryDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, paymentHistoryDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaymentHistoryDTO{" +
            "id=" + getId() +
            ", organizationName='" + getOrganizationName() + "'" +
            ", serviceName='" + getServiceName() + "'" +
            ", groupsName='" + getGroupsName() + "'" +
            ", userId=" + getUserId() +
            ", paymentFrom='" + getPaymentFrom() + "'" +
            ", paymentTo='" + getPaymentTo() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            "}";
    }
}
