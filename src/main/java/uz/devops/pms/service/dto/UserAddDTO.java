package uz.devops.pms.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link uz.devops.pms.domain.UserAdd} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UserAddDTO implements Serializable {

    private Long id;

    private Double account;

    private UserDTO user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAccount() {
        return account;
    }

    public void setAccount(Double account) {
        this.account = account;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserAddDTO)) {
            return false;
        }

        UserAddDTO userAddDTO = (UserAddDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, userAddDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserAddDTO{" +
            "id=" + getId() +
            ", account=" + getAccount() +
            ", user=" + getUser() +
            "}";
    }
}
