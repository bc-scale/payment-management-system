package uz.devops.pms.service.dto;

public class LoanDTO {

    private String debtor;

    private String serviceName;

    private String groupName;

    private String organizationName;

    private String dateFrom;

    private Double amount;

    private Double price;

    public LoanDTO(
        String debtor,
        String serviceName,
        String groupName,
        String organizationName,
        String dateFrom,
        Double amount,
        Double price
    ) {
        this.debtor = debtor;
        this.serviceName = serviceName;
        this.groupName = groupName;
        this.organizationName = organizationName;
        this.dateFrom = dateFrom;
        this.amount = amount;
        this.price = price;
    }

    public String getDebtor() {
        return debtor;
    }

    public void setDebtor(String debtor) {
        this.debtor = debtor;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
