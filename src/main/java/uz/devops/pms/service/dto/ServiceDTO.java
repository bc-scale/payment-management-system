package uz.devops.pms.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;
import uz.devops.pms.domain.enumeration.ChronometricType;
import uz.devops.pms.domain.enumeration.PeriodType;

/**
 * A DTO for the {@link uz.devops.pms.domain.Service} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ServiceDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Double price;

    @NotNull
    private PeriodType periodType;

    private ChronometricType chronometricType;

    private Integer periodCount;

    @NotNull
    private LocalDate startDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public PeriodType getPeriodType() {
        return periodType;
    }

    public void setPeriodType(PeriodType periodType) {
        this.periodType = periodType;
    }

    public ChronometricType getChronometricType() {
        return chronometricType;
    }

    public void setChronometricType(ChronometricType chronometricType) {
        this.chronometricType = chronometricType;
    }

    public Integer getPeriodCount() {
        return periodCount;
    }

    public void setPeriodCount(Integer periodCount) {
        this.periodCount = periodCount;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ServiceDTO)) {
            return false;
        }

        ServiceDTO serviceDTO = (ServiceDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, serviceDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ServiceDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", price=" + getPrice() +
            ", periodType='" + getPeriodType() + "'" +
            ", chronometricType='" + getChronometricType() + "'" +
            ", periodCount=" + getPeriodCount() +
            ", startDate='" + getStartDate() + "'" +
            "}";
    }
}
