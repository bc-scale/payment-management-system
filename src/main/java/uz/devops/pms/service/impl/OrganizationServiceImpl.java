package uz.devops.pms.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.devops.pms.domain.Organization;
import uz.devops.pms.domain.User;
import uz.devops.pms.repository.OrganizationRepository;
import uz.devops.pms.security.AuthoritiesConstants;
import uz.devops.pms.security.CommonUtils;
import uz.devops.pms.security.SecurityUtils;
import uz.devops.pms.service.OrganizationService;
import uz.devops.pms.service.UserService;
import uz.devops.pms.service.dto.OrganizationDTO;
import uz.devops.pms.service.mapper.OrganizationMapper;

/**
 * Service Implementation for managing {@link Organization}.
 */
@Service
@Transactional
public class OrganizationServiceImpl implements OrganizationService {

    private final Logger log = LoggerFactory.getLogger(OrganizationServiceImpl.class);

    private final OrganizationRepository organizationRepository;

    private final OrganizationMapper organizationMapper;

    private final UserService userService;

    private final CommonUtils utils;

    public OrganizationServiceImpl(
        OrganizationRepository organizationRepository,
        OrganizationMapper organizationMapper,
        UserService userService,
        CommonUtils utils
    ) {
        this.organizationRepository = organizationRepository;
        this.organizationMapper = organizationMapper;
        this.userService = userService;
        this.utils = utils;
    }

    @Override
    public OrganizationDTO save(OrganizationDTO organizationDTO) {
        log.debug("Request to save Organization : {}", organizationDTO);
        if (organizationDTO.getAccount() < 0) throw new IllegalArgumentException("Invalid organization account");
        if (organizationRepository.findByName(organizationDTO.getName()).isPresent()) {
            throw new IllegalArgumentException("Organization exists");
        }
        Organization organization = organizationMapper.toEntity(organizationDTO);
        organization = organizationRepository.save(organization);
        userService.addRole(organizationDTO.getOwner().getId(), AuthoritiesConstants.ORGANIZATION_OWNER);
        return organizationMapper.toDto(organization);
    }

    @Override
    public OrganizationDTO update(OrganizationDTO organizationDTO) {
        log.debug("Request to update Organization : {}", organizationDTO);
        Organization organization = organizationMapper.toEntity(organizationDTO);
        organization = organizationRepository.save(organization);
        return organizationMapper.toDto(organization);
    }

    @Override
    public Optional<OrganizationDTO> partialUpdate(OrganizationDTO organizationDTO) {
        log.debug("Request to partially update Organization : {}", organizationDTO);

        return organizationRepository
            .findById(organizationDTO.getId())
            .map(existingOrganization -> {
                organizationMapper.partialUpdate(existingOrganization, organizationDTO);

                return existingOrganization;
            })
            .map(organizationRepository::save)
            .map(organizationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<OrganizationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Organizations");
        return organizationRepository.findAll(pageable).map(organizationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrganizationDTO> findAllForOwner(Long id) {
        log.debug("Request to get all Organizations");
        return organizationRepository.findByOwnerIsCurrentUser().stream().map(organizationMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<OrganizationDTO> findOne(Long id) {
        log.debug("Request to get Organization : {}", id);
        return organizationRepository.findById(id).map(organizationMapper::toDto);
    }

    @Override
    public Optional<OrganizationDTO> delete(Long id) {
        log.debug("Request to delete Organization : {}", id);
        Organization organization = organizationRepository
            .findById(id)
            .orElseThrow(() -> {
                throw new IllegalArgumentException();
            });
        organizationRepository.deleteById(id);
        return Optional.of(organizationMapper.toDto(organization));
    }
}
