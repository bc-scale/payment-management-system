package uz.devops.pms.service.impl;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.devops.pms.domain.*;
import uz.devops.pms.domain.enumeration.ChronometricType;
import uz.devops.pms.domain.enumeration.PeriodType;
import uz.devops.pms.repository.*;
import uz.devops.pms.service.GroupsService;
import uz.devops.pms.service.PaymentService;
import uz.devops.pms.service.ServiceService;
import uz.devops.pms.service.dto.*;
import uz.devops.pms.service.mapper.PaymentMapper;

/**
 * Service Implementation for managing {@link Payment}.
 */
@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {

    private final Logger log = LoggerFactory.getLogger(PaymentServiceImpl.class);

    private final PaymentRepository paymentRepository;

    private final PaymentMapper paymentMapper;

    private final ServiceService serviceService;

    private final GroupsService groupsService;

    private final GroupsRepository groupsRepository;

    private final OrganizationRepository organizationRepository;

    private final UserAddRepository userAddRepository;

    private final UserRepository userRepository;

    public PaymentServiceImpl(
        PaymentRepository paymentRepository,
        PaymentMapper paymentMapper,
        ServiceService serviceService,
        GroupsService groupsService,
        GroupsRepository groupsRepository,
        OrganizationRepository organizationRepository,
        UserAddRepository userAddRepository,
        UserRepository userRepository
    ) {
        this.paymentRepository = paymentRepository;
        this.paymentMapper = paymentMapper;
        this.serviceService = serviceService;
        this.groupsService = groupsService;
        this.groupsRepository = groupsRepository;
        this.organizationRepository = organizationRepository;
        this.userAddRepository = userAddRepository;
        this.userRepository = userRepository;
    }

    @Override
    public PaymentDTO save(PaymentDTO paymentDTO) {
        log.debug("Request to save Payment : {}", paymentDTO);
        Payment payment = paymentMapper.toEntity(paymentDTO);
        ServiceDTO service = serviceService.findOne(paymentDTO.getService().getId()).orElseThrow();
        GroupsDTO group = groupsService.findOne(payment.getGroup().getId()).orElseThrow();
        User payer = userRepository.findById(payment.getPayer().getId()).orElseThrow();
        UserAdd userAdd = userAddRepository.findByUserId(payer.getId());

        payment.setPrice(service.getPrice());
        Payment lastPayment = paymentRepository.findFirstByPayerIdAndServiceIdOrderByPaymentToDesc(payer.getId(), service.getId());

        if (group.getServices().stream().map(ServiceDTO::getId).noneMatch(aLong -> Objects.equals(aLong, payment.getService().getId()))) {
            throw new IllegalArgumentException("Service and Group not match");
        }

        if (service.getPeriodType() == PeriodType.ONE_TIME) {
            payment.setPaymentTo(payment.getService().getStartDate());
            payment.setPaymentFrom(payment.getService().getStartDate());
            if (lastPayment != null) {
                if (Objects.equals(lastPayment.getPrice(), lastPayment.getPayedMoney())) {
                    throw new IllegalArgumentException("Payment already payed");
                } else {
                    if (lastPayment.getPayedMoney() + payment.getPayedMoney() <= lastPayment.getPrice()) {
                        lastPayment.setPayedMoney(payment.getPayedMoney() + lastPayment.getPayedMoney());
                        paymentRepository.save(lastPayment);
                        return paymentDTO;
                    }
                    payment.setPayedMoney(payment.getPayedMoney() - (lastPayment.getPrice() - payment.getPayedMoney()));
                    lastPayment.setPayedMoney(lastPayment.getPrice());
                    userAdd.setAccount(userAdd.getAccount() + payment.getPayedMoney());
                    userAddRepository.save(userAdd);
                    paymentRepository.save(lastPayment);
                    return paymentDTO;
                }
            }
            if (payment.getPrice() < payment.getPayedMoney()) {
                userAdd.setAccount(userAdd.getAccount() + payment.getPayedMoney() - payment.getPrice());
                userAddRepository.save(userAdd);
                payment.setPayedMoney(payment.getPrice());
            }
            paymentRepository.save(payment);
        }

        if (service.getPeriodType() == PeriodType.REGULAR) {
            setPaymentFromAndTo(payment, service.getChronometricType(), service.getPeriodCount());
            if (lastPayment != null && !Objects.equals(lastPayment.getPrice(), lastPayment.getPayedMoney())) {
                payment.setPaymentFrom(lastPayment.getPaymentTo());
                if (lastPayment.getPayedMoney() + payment.getPayedMoney() > lastPayment.getPrice()) {
                    payment.setPayedMoney(payment.getPayedMoney() - (lastPayment.getPrice() - lastPayment.getPayedMoney()));
                    lastPayment.setPayedMoney(lastPayment.getPrice());
                    paymentRepository.save(lastPayment);
                } else {
                    lastPayment.setPayedMoney(lastPayment.getPayedMoney() + payment.getPayedMoney());
                    paymentRepository.save(lastPayment);
                    return paymentDTO;
                }
            }
            List<Payment> payments = ifMoneyIsMore(payment);
            paymentRepository.saveAll(payments);
        }

        return paymentMapper.toDto(payment);
    }

    private List<Payment> ifMoneyIsMore(Payment payment) {
        List<Payment> paymentList = new ArrayList<>();
        while (payment.getPayedMoney() > payment.getPrice()) {
            Payment paymentT = new Payment();
            paymentT.setPaymentFrom(payment.getPaymentFrom());
            paymentT.setPaymentTo(payment.getPaymentTo());
            paymentT.setService(payment.getService());
            paymentT.setPrice(payment.getPrice());
            paymentT.setGroup(payment.getGroup());
            paymentT.setPayedMoney(payment.getPrice());
            paymentT.setPayer(payment.getPayer());
            payment.setPayedMoney(payment.getPayedMoney() - payment.getPrice());
            payment.setPaymentFrom(payment.getPaymentTo());
            setPaymentFromAndTo(payment, payment.getService().getChronometricType(), payment.getService().getPeriodCount());
            paymentList.add(paymentT);
        }

        Payment paymentT = new Payment();
        paymentT.setService(payment.getService());
        paymentT.setPrice(payment.getPrice());
        paymentT.setGroup(payment.getGroup());
        paymentT.setPayedMoney(payment.getPayedMoney());
        paymentT.setPayer(payment.getPayer());
        paymentT.setPaymentFrom(payment.getPaymentFrom());
        paymentT.setPaymentTo(payment.getPaymentTo());
        paymentList.add(paymentT);

        return paymentList;
    }

    private void setPaymentFromAndTo(Payment payment, ChronometricType type, int amount) {
        switch (type) {
            case DAY:
                payment.setPaymentTo(payment.getPaymentTo().plusDays(amount));
                break;
            case WEAK:
                payment.setPaymentTo(payment.getPaymentFrom().plusWeeks(amount));
                break;
            case MONTH:
                payment.setPaymentTo(payment.getPaymentFrom().plusMonths(amount));
                break;
            case YEAR:
                payment.setPaymentTo(payment.getPaymentFrom().plusYears(amount));
                break;
        }
    }

    @Override
    public PaymentDTO update(PaymentDTO paymentDTO) {
        log.debug("Request to update Payment : {}", paymentDTO);
        Payment payment = paymentMapper.toEntity(paymentDTO);
        payment = paymentRepository.save(payment);
        return paymentMapper.toDto(payment);
    }

    @Override
    public Optional<PaymentDTO> partialUpdate(PaymentDTO paymentDTO) {
        log.debug("Request to partially update Payment : {}", paymentDTO);

        return paymentRepository
            .findById(paymentDTO.getId())
            .map(existingPayment -> {
                paymentMapper.partialUpdate(existingPayment, paymentDTO);

                return existingPayment;
            })
            .map(paymentRepository::save)
            .map(paymentMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PaymentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Payments");
        return paymentRepository.findAll(pageable).map(paymentMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PaymentDTO> findOne(Long id) {
        log.debug("Request to get Payment : {}", id);
        return paymentRepository.findById(id).map(paymentMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Payment : {}", id);
        paymentRepository.deleteById(id);
    }

    @Override
    public List<LoanDTO> findLoanForOrganization(Long id) {
        Set<Groups> groups = organizationRepository.findById(id).orElseThrow().getGroups();
        List<LoanDTO> loans = new ArrayList<>();
        for (Groups group : groups) {
            loans.addAll(findLoanForGroup(group.getId()));
        }
        return loans;
    }

    @Override
    public List<LoanDTO> findLoanForGroup(Long id) {
        Groups group = groupsRepository.findById(id).orElseThrow();
        Set<UserAdd> users = group.getUsers();
        List<LoanDTO> loans = new ArrayList<>();
        Set<uz.devops.pms.domain.Service> services = group.getServices();
        for (UserAdd user : users) {
            for (uz.devops.pms.domain.Service service : services) {
                if (service.getPeriodType() == PeriodType.ONE_TIME) {
                    Payment payment = paymentRepository.findFirstByPayerIdAndServiceIdOrderByPaymentToDesc(
                        user.getUser().getId(),
                        service.getId()
                    );
                    if (payment == null) {
                        loans.add(
                            new LoanDTO(
                                user.getUser().getLogin(),
                                service.getName(),
                                group.getName(),
                                group.getOrganization().getName(),
                                service.getStartDate().toString(),
                                service.getPrice(),
                                service.getPrice()
                            )
                        );
                    } else if (payment.getPayedMoney() < payment.getPrice()) {
                        loans.add(
                            new LoanDTO(
                                user.getUser().getLogin(),
                                service.getName(),
                                group.getName(),
                                group.getOrganization().getName(),
                                service.getStartDate().toString(),
                                payment.getPrice() - payment.getPayedMoney(),
                                service.getPrice()
                            )
                        );
                    }
                } else if (service.getPeriodType() == PeriodType.REGULAR) {
                    List<Payment> payments = paymentRepository.findAllByPayerIdAndServiceId(user.getUser().getId(), service.getId());
                    Payment payment = null;
                    if (payments.size() == 0) {
                        payment = new Payment();
                        payment.setService(service);
                        payment.setPrice(service.getPrice());
                        payment.setPayedMoney(0D);
                        payment.setPaymentFrom(service.getStartDate());
                        setPaymentFromAndTo(payment, service.getChronometricType(), service.getPeriodCount());
                    } else {
                        payment =
                            paymentRepository.findFirstByPayerIdAndServiceIdOrderByPaymentToDesc(user.getUser().getId(), service.getId());
                    }
                    while (payment.getPaymentFrom().isBefore(LocalDate.now())) {
                        loans.add(
                            new LoanDTO(
                                user.getUser().getLogin(),
                                service.getName(),
                                group.getName(),
                                group.getOrganization().getName(),
                                payment.getPaymentFrom().toString(),
                                payment.getPrice() - payment.getPayedMoney(),
                                service.getPrice()
                            )
                        );
                        payment.setPaymentFrom(payment.getPaymentTo());
                        setPaymentFromAndTo(payment, payment.getService().getChronometricType(), payment.getService().getPeriodCount());
                    }
                }
            }
        }
        return loans;
    }

    @Override
    public List<LoanDTO> findLoanForIndividual(Long id) {
        User user = userRepository.findById(id).orElseThrow();
        UserAdd byUserId = userAddRepository.findByUserId(user.getId());
        Set<Groups> groups = byUserId.getGroups();
        List<LoanDTO> loans = new ArrayList<>();
        for (Groups group : groups) {
            loans.addAll(
                findLoanForGroup(group.getId())
                    .stream()
                    .filter(loanDTO -> loanDTO.getDebtor().equals(user.getLogin()))
                    .collect(Collectors.toList())
            );
        }
        return loans;
    }
}
