package uz.devops.pms.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.devops.pms.domain.UserAdd;
import uz.devops.pms.repository.UserAddRepository;
import uz.devops.pms.service.UserAddService;
import uz.devops.pms.service.dto.UserAddDTO;
import uz.devops.pms.service.mapper.UserAddMapper;

/**
 * Service Implementation for managing {@link UserAdd}.
 */
@Service
@Transactional
public class UserAddServiceImpl implements UserAddService {

    private final Logger log = LoggerFactory.getLogger(UserAddServiceImpl.class);

    private final UserAddRepository userAddRepository;

    private final UserAddMapper userAddMapper;

    public UserAddServiceImpl(UserAddRepository userAddRepository, UserAddMapper userAddMapper) {
        this.userAddRepository = userAddRepository;
        this.userAddMapper = userAddMapper;
    }

    @Override
    public UserAddDTO save(UserAddDTO userAddDTO) {
        log.debug("Request to save UserAdd : {}", userAddDTO);
        UserAdd userAdd = userAddMapper.toEntity(userAddDTO);
        userAdd = userAddRepository.save(userAdd);
        return userAddMapper.toDto(userAdd);
    }

    @Override
    public UserAddDTO update(UserAddDTO userAddDTO) {
        log.debug("Request to update UserAdd : {}", userAddDTO);
        UserAdd userAdd = userAddMapper.toEntity(userAddDTO);
        userAdd = userAddRepository.save(userAdd);
        return userAddMapper.toDto(userAdd);
    }

    @Override
    public Optional<UserAddDTO> partialUpdate(UserAddDTO userAddDTO) {
        log.debug("Request to partially update UserAdd : {}", userAddDTO);

        return userAddRepository
            .findById(userAddDTO.getId())
            .map(existingUserAdd -> {
                userAddMapper.partialUpdate(existingUserAdd, userAddDTO);

                return existingUserAdd;
            })
            .map(userAddRepository::save)
            .map(userAddMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserAddDTO> findAll() {
        log.debug("Request to get all UserAdds");
        return userAddRepository.findAll().stream().map(userAddMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserAddDTO> findOne(Long id) {
        log.debug("Request to get UserAdd : {}", id);
        return userAddRepository.findById(id).map(userAddMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserAdd : {}", id);
        userAddRepository.deleteById(id);
    }
}
