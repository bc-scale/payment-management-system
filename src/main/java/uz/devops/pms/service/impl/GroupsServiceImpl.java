package uz.devops.pms.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.devops.pms.domain.Groups;
import uz.devops.pms.repository.GroupsRepository;
import uz.devops.pms.repository.OrganizationRepository;
import uz.devops.pms.security.AuthoritiesConstants;
import uz.devops.pms.service.GroupsService;
import uz.devops.pms.service.UserService;
import uz.devops.pms.service.dto.GroupsDTO;
import uz.devops.pms.service.mapper.GroupsMapper;
import uz.devops.pms.service.mapper.OrganizationMapper;

/**
 * Service Implementation for managing {@link Groups}.
 */
@Service
@Transactional
public class GroupsServiceImpl implements GroupsService {

    private final Logger log = LoggerFactory.getLogger(GroupsServiceImpl.class);

    private final GroupsRepository groupsRepository;

    private final GroupsMapper groupsMapper;

    private final UserService userService;

    public GroupsServiceImpl(
        GroupsRepository groupsRepository,
        GroupsMapper groupsMapper,
        OrganizationRepository organizationRepository,
        OrganizationMapper organizationMapper,
        UserService userService
    ) {
        this.groupsRepository = groupsRepository;
        this.groupsMapper = groupsMapper;
        this.userService = userService;
    }

    @Override
    public GroupsDTO save(GroupsDTO groupsDTO) {
        log.debug("Request to save Groups : {}", groupsDTO);
        Groups groups = groupsMapper.toEntity(groupsDTO);
        groups = groupsRepository.save(groups);
        //userService.addRole(groups.getManager().getId(), AuthoritiesConstants.MANAGER);
        return groupsMapper.toDto(groups);
    }

    @Override
    public GroupsDTO update(GroupsDTO groupsDTO) {
        log.debug("Request to update Groups : {}", groupsDTO);
        Groups groups = groupsMapper.toEntity(groupsDTO);
        groups = groupsRepository.save(groups);
        return groupsMapper.toDto(groups);
    }

    @Override
    public Optional<GroupsDTO> partialUpdate(GroupsDTO groupsDTO) {
        log.debug("Request to partially update Groups : {}", groupsDTO);

        return groupsRepository
            .findById(groupsDTO.getId())
            .map(existingGroups -> {
                groupsMapper.partialUpdate(existingGroups, groupsDTO);

                return existingGroups;
            })
            .map(groupsRepository::save)
            .map(groupsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<GroupsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Groups");
        return groupsRepository.findAll(pageable).map(groupsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<GroupsDTO> findAllForManager() {
        log.debug("Request to get all Groups");
        return groupsRepository.findByManagerIsCurrentUser().stream().map(groupsMapper::toDto).collect(Collectors.toList());
    }

    public Page<GroupsDTO> findAllWithEagerRelationships(Pageable pageable) {
        return groupsRepository.findAllWithEagerRelationships(pageable).map(groupsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<GroupsDTO> findOne(Long id) {
        log.debug("Request to get Groups : {}", id);
        return groupsRepository.findOneWithEagerRelationships(id).map(groupsMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Groups : {}", id);
        groupsRepository.deleteById(id);
    }
}
