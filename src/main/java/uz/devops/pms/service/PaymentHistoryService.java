package uz.devops.pms.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import uz.devops.pms.service.dto.PaymentHistoryDTO;

/**
 * Service Interface for managing {@link uz.devops.pms.domain.PaymentHistory}.
 */
public interface PaymentHistoryService {
    /**
     * Save a paymentHistory.
     *
     * @param paymentHistoryDTO the entity to save.
     * @return the persisted entity.
     */
    PaymentHistoryDTO save(PaymentHistoryDTO paymentHistoryDTO);

    /**
     * Updates a paymentHistory.
     *
     * @param paymentHistoryDTO the entity to update.
     * @return the persisted entity.
     */
    PaymentHistoryDTO update(PaymentHistoryDTO paymentHistoryDTO);

    /**
     * Partially updates a paymentHistory.
     *
     * @param paymentHistoryDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PaymentHistoryDTO> partialUpdate(PaymentHistoryDTO paymentHistoryDTO);

    /**
     * Get all the paymentHistories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PaymentHistoryDTO> findAll(Pageable pageable);

    /**
     * Get the "id" paymentHistory.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PaymentHistoryDTO> findOne(Long id);

    /**
     * Delete the "id" paymentHistory.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
