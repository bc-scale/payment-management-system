package uz.devops.pms.service.mapper;

import org.mapstruct.*;
import uz.devops.pms.domain.Groups;
import uz.devops.pms.domain.Payment;
import uz.devops.pms.domain.Service;
import uz.devops.pms.domain.User;
import uz.devops.pms.service.dto.GroupsDTO;
import uz.devops.pms.service.dto.PaymentDTO;
import uz.devops.pms.service.dto.ServiceDTO;
import uz.devops.pms.service.dto.UserDTO;

/**
 * Mapper for the entity {@link Payment} and its DTO {@link PaymentDTO}.
 */
@Mapper(componentModel = "spring")
public interface PaymentMapper extends EntityMapper<PaymentDTO, Payment> {
    @Mapping(target = "payer", source = "payer", qualifiedByName = "userId")
    @Mapping(target = "service", source = "service", qualifiedByName = "serviceId")
    @Mapping(target = "group", source = "group", qualifiedByName = "groupsId")
    PaymentDTO toDto(Payment s);

    @Named("userId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    UserDTO toDtoUserId(User user);

    @Named("serviceId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ServiceDTO toDtoServiceId(Service service);

    @Named("groupsId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    GroupsDTO toDtoGroupsId(Groups groups);
}
