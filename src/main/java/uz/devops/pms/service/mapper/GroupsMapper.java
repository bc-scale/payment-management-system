package uz.devops.pms.service.mapper;

import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;
import uz.devops.pms.domain.Groups;
import uz.devops.pms.domain.Organization;
import uz.devops.pms.domain.Service;
import uz.devops.pms.domain.User;
import uz.devops.pms.domain.UserAdd;
import uz.devops.pms.service.dto.GroupsDTO;
import uz.devops.pms.service.dto.OrganizationDTO;
import uz.devops.pms.service.dto.ServiceDTO;
import uz.devops.pms.service.dto.UserAddDTO;
import uz.devops.pms.service.dto.UserDTO;

/**
 * Mapper for the entity {@link Groups} and its DTO {@link GroupsDTO}.
 */
@Mapper(componentModel = "spring")
public interface GroupsMapper extends EntityMapper<GroupsDTO, Groups> {
    @Mapping(target = "users", source = "users", qualifiedByName = "userAddIdSet")
    @Mapping(target = "services", source = "services", qualifiedByName = "serviceIdSet")
    GroupsDTO toDto(Groups s);

    @Mapping(target = "removeUsers", ignore = true)
    @Mapping(target = "removeServices", ignore = true)
    Groups toEntity(GroupsDTO groupsDTO);

    @Named("userId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    UserDTO toDtoUserId(User user);

    @Named("organizationId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    OrganizationDTO toDtoOrganizationId(Organization organization);

    @Named("userAddId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    UserAddDTO toDtoUserAddId(UserAdd userAdd);

    @Named("userAddIdSet")
    default Set<UserAddDTO> toDtoUserAddIdSet(Set<UserAdd> userAdd) {
        return userAdd.stream().map(this::toDtoUserAddId).collect(Collectors.toSet());
    }

    @Named("serviceId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ServiceDTO toDtoServiceId(Service service);

    @Named("serviceIdSet")
    default Set<ServiceDTO> toDtoServiceIdSet(Set<Service> service) {
        return service.stream().map(this::toDtoServiceId).collect(Collectors.toSet());
    }
}
