package uz.devops.pms.service.mapper;

import org.mapstruct.*;
import uz.devops.pms.domain.User;
import uz.devops.pms.domain.UserAdd;
import uz.devops.pms.service.dto.UserAddDTO;
import uz.devops.pms.service.dto.UserDTO;

/**
 * Mapper for the entity {@link UserAdd} and its DTO {@link UserAddDTO}.
 */
@Mapper(componentModel = "spring")
public interface UserAddMapper extends EntityMapper<UserAddDTO, UserAdd> {
    @Mapping(target = "user", source = "user", qualifiedByName = "userId")
    UserAddDTO toDto(UserAdd s);

    @Named("userId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    UserDTO toDtoUserId(User user);
}
