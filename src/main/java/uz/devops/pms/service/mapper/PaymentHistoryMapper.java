package uz.devops.pms.service.mapper;

import org.mapstruct.*;
import uz.devops.pms.domain.PaymentHistory;
import uz.devops.pms.service.dto.PaymentHistoryDTO;

/**
 * Mapper for the entity {@link PaymentHistory} and its DTO {@link PaymentHistoryDTO}.
 */
@Mapper(componentModel = "spring")
public interface PaymentHistoryMapper extends EntityMapper<PaymentHistoryDTO, PaymentHistory> {}
