package uz.devops.pms.service.mapper;

import org.mapstruct.*;
import uz.devops.pms.domain.Service;
import uz.devops.pms.service.dto.ServiceDTO;

/**
 * Mapper for the entity {@link Service} and its DTO {@link ServiceDTO}.
 */
@Mapper(componentModel = "spring")
public interface ServiceMapper extends EntityMapper<ServiceDTO, Service> {}
