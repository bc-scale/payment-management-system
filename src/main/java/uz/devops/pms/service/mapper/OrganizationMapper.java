package uz.devops.pms.service.mapper;

import org.mapstruct.*;
import uz.devops.pms.domain.Organization;
import uz.devops.pms.domain.User;
import uz.devops.pms.service.dto.OrganizationDTO;
import uz.devops.pms.service.dto.UserDTO;

/**
 * Mapper for the entity {@link Organization} and its DTO {@link OrganizationDTO}.
 */
@Mapper(componentModel = "spring")
public interface OrganizationMapper extends EntityMapper<OrganizationDTO, Organization> {
    OrganizationDTO toDto(Organization s);

    @Named("userId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    UserDTO toDtoUserId(User user);
}
