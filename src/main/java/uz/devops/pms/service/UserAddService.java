package uz.devops.pms.service;

import java.util.List;
import java.util.Optional;
import uz.devops.pms.service.dto.UserAddDTO;

/**
 * Service Interface for managing {@link uz.devops.pms.domain.UserAdd}.
 */
public interface UserAddService {
    /**
     * Save a userAdd.
     *
     * @param userAddDTO the entity to save.
     * @return the persisted entity.
     */
    UserAddDTO save(UserAddDTO userAddDTO);

    /**
     * Updates a userAdd.
     *
     * @param userAddDTO the entity to update.
     * @return the persisted entity.
     */
    UserAddDTO update(UserAddDTO userAddDTO);

    /**
     * Partially updates a userAdd.
     *
     * @param userAddDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<UserAddDTO> partialUpdate(UserAddDTO userAddDTO);

    /**
     * Get all the userAdds.
     *
     * @return the list of entities.
     */
    List<UserAddDTO> findAll();

    /**
     * Get the "id" userAdd.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserAddDTO> findOne(Long id);

    /**
     * Delete the "id" userAdd.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
