package uz.devops.pms.service;

import org.springframework.stereotype.Component;

@Component
public interface NotificationSender {
    void sent();
}
