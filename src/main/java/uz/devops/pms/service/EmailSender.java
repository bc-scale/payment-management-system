package uz.devops.pms.service;

import java.time.LocalDate;
import java.util.List;
import org.springframework.scheduling.annotation.Scheduled;
import uz.devops.pms.domain.Payment;
import uz.devops.pms.domain.enumeration.PeriodType;
import uz.devops.pms.repository.PaymentRepository;

public class EmailSender implements NotificationSender {

    private final MailService mailService;

    private final PaymentRepository paymentRepository;

    public EmailSender(MailService mailService, PaymentRepository paymentRepository) {
        this.mailService = mailService;
        this.paymentRepository = paymentRepository;
    }

    @Override
    @Scheduled(cron = "0 0 1 * * ?")
    public void sent() {
        List<Payment> all = paymentRepository.findAll();
        all.forEach(payment -> {
            if (payment.getService().getPeriodType() == PeriodType.REGULAR) {
                if (payment.getPaymentTo().plusDays(3).isAfter(LocalDate.now())) {
                    {
                        mailService.sendEmail(
                            payment.getPayer().getEmail(),
                            "Payment",
                            "Please pay for the service " + payment.getService().getName(),
                            false,
                            false
                        );
                    }
                }
            }
        });
    }
}
