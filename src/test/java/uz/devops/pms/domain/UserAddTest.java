package uz.devops.pms.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import uz.devops.pms.web.rest.TestUtil;

class UserAddTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserAdd.class);
        UserAdd userAdd1 = new UserAdd();
        userAdd1.setId(1L);
        UserAdd userAdd2 = new UserAdd();
        userAdd2.setId(userAdd1.getId());
        assertThat(userAdd1).isEqualTo(userAdd2);
        userAdd2.setId(2L);
        assertThat(userAdd1).isNotEqualTo(userAdd2);
        userAdd1.setId(null);
        assertThat(userAdd1).isNotEqualTo(userAdd2);
    }
}
