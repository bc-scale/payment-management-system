package uz.devops.pms.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import uz.devops.pms.web.rest.TestUtil;

class UserAddDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserAddDTO.class);
        UserAddDTO userAddDTO1 = new UserAddDTO();
        userAddDTO1.setId(1L);
        UserAddDTO userAddDTO2 = new UserAddDTO();
        assertThat(userAddDTO1).isNotEqualTo(userAddDTO2);
        userAddDTO2.setId(userAddDTO1.getId());
        assertThat(userAddDTO1).isEqualTo(userAddDTO2);
        userAddDTO2.setId(2L);
        assertThat(userAddDTO1).isNotEqualTo(userAddDTO2);
        userAddDTO1.setId(null);
        assertThat(userAddDTO1).isNotEqualTo(userAddDTO2);
    }
}
