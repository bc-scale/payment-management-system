package uz.devops.pms.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.devops.pms.IntegrationTest;
import uz.devops.pms.domain.PaymentHistory;
import uz.devops.pms.repository.PaymentHistoryRepository;
import uz.devops.pms.service.dto.PaymentHistoryDTO;
import uz.devops.pms.service.mapper.PaymentHistoryMapper;

/**
 * Integration tests for the {@link PaymentHistoryResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PaymentHistoryResourceIT {

    private static final String DEFAULT_ORGANIZATION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ORGANIZATION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SERVICE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SERVICE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_GROUPS_NAME = "AAAAAAAAAA";
    private static final String UPDATED_GROUPS_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;

    private static final LocalDate DEFAULT_PAYMENT_FROM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PAYMENT_FROM = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_PAYMENT_TO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PAYMENT_TO = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/payment-histories";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PaymentHistoryRepository paymentHistoryRepository;

    @Autowired
    private PaymentHistoryMapper paymentHistoryMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPaymentHistoryMockMvc;

    private PaymentHistory paymentHistory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentHistory createEntity(EntityManager em) {
        PaymentHistory paymentHistory = new PaymentHistory()
            .organizationName(DEFAULT_ORGANIZATION_NAME)
            .serviceName(DEFAULT_SERVICE_NAME)
            .groupsName(DEFAULT_GROUPS_NAME)
            .userId(DEFAULT_USER_ID)
            .paymentFrom(DEFAULT_PAYMENT_FROM)
            .paymentTo(DEFAULT_PAYMENT_TO)
            .createdAt(DEFAULT_CREATED_AT);
        return paymentHistory;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentHistory createUpdatedEntity(EntityManager em) {
        PaymentHistory paymentHistory = new PaymentHistory()
            .organizationName(UPDATED_ORGANIZATION_NAME)
            .serviceName(UPDATED_SERVICE_NAME)
            .groupsName(UPDATED_GROUPS_NAME)
            .userId(UPDATED_USER_ID)
            .paymentFrom(UPDATED_PAYMENT_FROM)
            .paymentTo(UPDATED_PAYMENT_TO)
            .createdAt(UPDATED_CREATED_AT);
        return paymentHistory;
    }

    @BeforeEach
    public void initTest() {
        paymentHistory = createEntity(em);
    }

    @Test
    @Transactional
    void createPaymentHistory() throws Exception {
        int databaseSizeBeforeCreate = paymentHistoryRepository.findAll().size();
        // Create the PaymentHistory
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);
        restPaymentHistoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isCreated());

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        PaymentHistory testPaymentHistory = paymentHistoryList.get(paymentHistoryList.size() - 1);
        assertThat(testPaymentHistory.getOrganizationName()).isEqualTo(DEFAULT_ORGANIZATION_NAME);
        assertThat(testPaymentHistory.getServiceName()).isEqualTo(DEFAULT_SERVICE_NAME);
        assertThat(testPaymentHistory.getGroupsName()).isEqualTo(DEFAULT_GROUPS_NAME);
        assertThat(testPaymentHistory.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testPaymentHistory.getPaymentFrom()).isEqualTo(DEFAULT_PAYMENT_FROM);
        assertThat(testPaymentHistory.getPaymentTo()).isEqualTo(DEFAULT_PAYMENT_TO);
        assertThat(testPaymentHistory.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
    }

    @Test
    @Transactional
    void createPaymentHistoryWithExistingId() throws Exception {
        // Create the PaymentHistory with an existing ID
        paymentHistory.setId(1L);
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        int databaseSizeBeforeCreate = paymentHistoryRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentHistoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkOrganizationNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentHistoryRepository.findAll().size();
        // set the field null
        paymentHistory.setOrganizationName(null);

        // Create the PaymentHistory, which fails.
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        restPaymentHistoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isBadRequest());

        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkServiceNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentHistoryRepository.findAll().size();
        // set the field null
        paymentHistory.setServiceName(null);

        // Create the PaymentHistory, which fails.
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        restPaymentHistoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isBadRequest());

        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkGroupsNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentHistoryRepository.findAll().size();
        // set the field null
        paymentHistory.setGroupsName(null);

        // Create the PaymentHistory, which fails.
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        restPaymentHistoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isBadRequest());

        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentHistoryRepository.findAll().size();
        // set the field null
        paymentHistory.setUserId(null);

        // Create the PaymentHistory, which fails.
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        restPaymentHistoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isBadRequest());

        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPaymentFromIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentHistoryRepository.findAll().size();
        // set the field null
        paymentHistory.setPaymentFrom(null);

        // Create the PaymentHistory, which fails.
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        restPaymentHistoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isBadRequest());

        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPaymentToIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentHistoryRepository.findAll().size();
        // set the field null
        paymentHistory.setPaymentTo(null);

        // Create the PaymentHistory, which fails.
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        restPaymentHistoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isBadRequest());

        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentHistoryRepository.findAll().size();
        // set the field null
        paymentHistory.setCreatedAt(null);

        // Create the PaymentHistory, which fails.
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        restPaymentHistoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isBadRequest());

        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPaymentHistories() throws Exception {
        // Initialize the database
        paymentHistoryRepository.saveAndFlush(paymentHistory);

        // Get all the paymentHistoryList
        restPaymentHistoryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paymentHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].organizationName").value(hasItem(DEFAULT_ORGANIZATION_NAME)))
            .andExpect(jsonPath("$.[*].serviceName").value(hasItem(DEFAULT_SERVICE_NAME)))
            .andExpect(jsonPath("$.[*].groupsName").value(hasItem(DEFAULT_GROUPS_NAME)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].paymentFrom").value(hasItem(DEFAULT_PAYMENT_FROM.toString())))
            .andExpect(jsonPath("$.[*].paymentTo").value(hasItem(DEFAULT_PAYMENT_TO.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())));
    }

    @Test
    @Transactional
    void getPaymentHistory() throws Exception {
        // Initialize the database
        paymentHistoryRepository.saveAndFlush(paymentHistory);

        // Get the paymentHistory
        restPaymentHistoryMockMvc
            .perform(get(ENTITY_API_URL_ID, paymentHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(paymentHistory.getId().intValue()))
            .andExpect(jsonPath("$.organizationName").value(DEFAULT_ORGANIZATION_NAME))
            .andExpect(jsonPath("$.serviceName").value(DEFAULT_SERVICE_NAME))
            .andExpect(jsonPath("$.groupsName").value(DEFAULT_GROUPS_NAME))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.intValue()))
            .andExpect(jsonPath("$.paymentFrom").value(DEFAULT_PAYMENT_FROM.toString()))
            .andExpect(jsonPath("$.paymentTo").value(DEFAULT_PAYMENT_TO.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()));
    }

    @Test
    @Transactional
    void getNonExistingPaymentHistory() throws Exception {
        // Get the paymentHistory
        restPaymentHistoryMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPaymentHistory() throws Exception {
        // Initialize the database
        paymentHistoryRepository.saveAndFlush(paymentHistory);

        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().size();

        // Update the paymentHistory
        PaymentHistory updatedPaymentHistory = paymentHistoryRepository.findById(paymentHistory.getId()).get();
        // Disconnect from session so that the updates on updatedPaymentHistory are not directly saved in db
        em.detach(updatedPaymentHistory);
        updatedPaymentHistory
            .organizationName(UPDATED_ORGANIZATION_NAME)
            .serviceName(UPDATED_SERVICE_NAME)
            .groupsName(UPDATED_GROUPS_NAME)
            .userId(UPDATED_USER_ID)
            .paymentFrom(UPDATED_PAYMENT_FROM)
            .paymentTo(UPDATED_PAYMENT_TO)
            .createdAt(UPDATED_CREATED_AT);
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(updatedPaymentHistory);

        restPaymentHistoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, paymentHistoryDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isOk());

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);
        PaymentHistory testPaymentHistory = paymentHistoryList.get(paymentHistoryList.size() - 1);
        assertThat(testPaymentHistory.getOrganizationName()).isEqualTo(UPDATED_ORGANIZATION_NAME);
        assertThat(testPaymentHistory.getServiceName()).isEqualTo(UPDATED_SERVICE_NAME);
        assertThat(testPaymentHistory.getGroupsName()).isEqualTo(UPDATED_GROUPS_NAME);
        assertThat(testPaymentHistory.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testPaymentHistory.getPaymentFrom()).isEqualTo(UPDATED_PAYMENT_FROM);
        assertThat(testPaymentHistory.getPaymentTo()).isEqualTo(UPDATED_PAYMENT_TO);
        assertThat(testPaymentHistory.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void putNonExistingPaymentHistory() throws Exception {
        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().size();
        paymentHistory.setId(count.incrementAndGet());

        // Create the PaymentHistory
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentHistoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, paymentHistoryDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPaymentHistory() throws Exception {
        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().size();
        paymentHistory.setId(count.incrementAndGet());

        // Create the PaymentHistory
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentHistoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPaymentHistory() throws Exception {
        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().size();
        paymentHistory.setId(count.incrementAndGet());

        // Create the PaymentHistory
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentHistoryMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePaymentHistoryWithPatch() throws Exception {
        // Initialize the database
        paymentHistoryRepository.saveAndFlush(paymentHistory);

        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().size();

        // Update the paymentHistory using partial update
        PaymentHistory partialUpdatedPaymentHistory = new PaymentHistory();
        partialUpdatedPaymentHistory.setId(paymentHistory.getId());

        partialUpdatedPaymentHistory
            .serviceName(UPDATED_SERVICE_NAME)
            .groupsName(UPDATED_GROUPS_NAME)
            .userId(UPDATED_USER_ID)
            .paymentFrom(UPDATED_PAYMENT_FROM)
            .paymentTo(UPDATED_PAYMENT_TO)
            .createdAt(UPDATED_CREATED_AT);

        restPaymentHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPaymentHistory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPaymentHistory))
            )
            .andExpect(status().isOk());

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);
        PaymentHistory testPaymentHistory = paymentHistoryList.get(paymentHistoryList.size() - 1);
        assertThat(testPaymentHistory.getOrganizationName()).isEqualTo(DEFAULT_ORGANIZATION_NAME);
        assertThat(testPaymentHistory.getServiceName()).isEqualTo(UPDATED_SERVICE_NAME);
        assertThat(testPaymentHistory.getGroupsName()).isEqualTo(UPDATED_GROUPS_NAME);
        assertThat(testPaymentHistory.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testPaymentHistory.getPaymentFrom()).isEqualTo(UPDATED_PAYMENT_FROM);
        assertThat(testPaymentHistory.getPaymentTo()).isEqualTo(UPDATED_PAYMENT_TO);
        assertThat(testPaymentHistory.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void fullUpdatePaymentHistoryWithPatch() throws Exception {
        // Initialize the database
        paymentHistoryRepository.saveAndFlush(paymentHistory);

        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().size();

        // Update the paymentHistory using partial update
        PaymentHistory partialUpdatedPaymentHistory = new PaymentHistory();
        partialUpdatedPaymentHistory.setId(paymentHistory.getId());

        partialUpdatedPaymentHistory
            .organizationName(UPDATED_ORGANIZATION_NAME)
            .serviceName(UPDATED_SERVICE_NAME)
            .groupsName(UPDATED_GROUPS_NAME)
            .userId(UPDATED_USER_ID)
            .paymentFrom(UPDATED_PAYMENT_FROM)
            .paymentTo(UPDATED_PAYMENT_TO)
            .createdAt(UPDATED_CREATED_AT);

        restPaymentHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPaymentHistory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPaymentHistory))
            )
            .andExpect(status().isOk());

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);
        PaymentHistory testPaymentHistory = paymentHistoryList.get(paymentHistoryList.size() - 1);
        assertThat(testPaymentHistory.getOrganizationName()).isEqualTo(UPDATED_ORGANIZATION_NAME);
        assertThat(testPaymentHistory.getServiceName()).isEqualTo(UPDATED_SERVICE_NAME);
        assertThat(testPaymentHistory.getGroupsName()).isEqualTo(UPDATED_GROUPS_NAME);
        assertThat(testPaymentHistory.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testPaymentHistory.getPaymentFrom()).isEqualTo(UPDATED_PAYMENT_FROM);
        assertThat(testPaymentHistory.getPaymentTo()).isEqualTo(UPDATED_PAYMENT_TO);
        assertThat(testPaymentHistory.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void patchNonExistingPaymentHistory() throws Exception {
        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().size();
        paymentHistory.setId(count.incrementAndGet());

        // Create the PaymentHistory
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, paymentHistoryDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPaymentHistory() throws Exception {
        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().size();
        paymentHistory.setId(count.incrementAndGet());

        // Create the PaymentHistory
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPaymentHistory() throws Exception {
        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().size();
        paymentHistory.setId(count.incrementAndGet());

        // Create the PaymentHistory
        PaymentHistoryDTO paymentHistoryDTO = paymentHistoryMapper.toDto(paymentHistory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(paymentHistoryDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePaymentHistory() throws Exception {
        // Initialize the database
        paymentHistoryRepository.saveAndFlush(paymentHistory);

        int databaseSizeBeforeDelete = paymentHistoryRepository.findAll().size();

        // Delete the paymentHistory
        restPaymentHistoryMockMvc
            .perform(delete(ENTITY_API_URL_ID, paymentHistory.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
