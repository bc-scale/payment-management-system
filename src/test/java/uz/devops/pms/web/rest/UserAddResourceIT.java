package uz.devops.pms.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.devops.pms.IntegrationTest;
import uz.devops.pms.domain.UserAdd;
import uz.devops.pms.repository.UserAddRepository;
import uz.devops.pms.service.dto.UserAddDTO;
import uz.devops.pms.service.mapper.UserAddMapper;

/**
 * Integration tests for the {@link UserAddResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class UserAddResourceIT {

    private static final Double DEFAULT_ACCOUNT = 1D;
    private static final Double UPDATED_ACCOUNT = 2D;

    private static final String ENTITY_API_URL = "/api/user-adds";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private UserAddRepository userAddRepository;

    @Autowired
    private UserAddMapper userAddMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserAddMockMvc;

    private UserAdd userAdd;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserAdd createEntity(EntityManager em) {
        UserAdd userAdd = new UserAdd().account(DEFAULT_ACCOUNT);
        return userAdd;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserAdd createUpdatedEntity(EntityManager em) {
        UserAdd userAdd = new UserAdd().account(UPDATED_ACCOUNT);
        return userAdd;
    }

    @BeforeEach
    public void initTest() {
        userAdd = createEntity(em);
    }

    @Test
    @Transactional
    void createUserAdd() throws Exception {
        int databaseSizeBeforeCreate = userAddRepository.findAll().size();
        // Create the UserAdd
        UserAddDTO userAddDTO = userAddMapper.toDto(userAdd);
        restUserAddMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userAddDTO)))
            .andExpect(status().isCreated());

        // Validate the UserAdd in the database
        List<UserAdd> userAddList = userAddRepository.findAll();
        assertThat(userAddList).hasSize(databaseSizeBeforeCreate + 1);
        UserAdd testUserAdd = userAddList.get(userAddList.size() - 1);
        assertThat(testUserAdd.getAccount()).isEqualTo(DEFAULT_ACCOUNT);
    }

    @Test
    @Transactional
    void createUserAddWithExistingId() throws Exception {
        // Create the UserAdd with an existing ID
        userAdd.setId(1L);
        UserAddDTO userAddDTO = userAddMapper.toDto(userAdd);

        int databaseSizeBeforeCreate = userAddRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserAddMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userAddDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserAdd in the database
        List<UserAdd> userAddList = userAddRepository.findAll();
        assertThat(userAddList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllUserAdds() throws Exception {
        // Initialize the database
        userAddRepository.saveAndFlush(userAdd);

        // Get all the userAddList
        restUserAddMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userAdd.getId().intValue())))
            .andExpect(jsonPath("$.[*].account").value(hasItem(DEFAULT_ACCOUNT.doubleValue())));
    }

    @Test
    @Transactional
    void getUserAdd() throws Exception {
        // Initialize the database
        userAddRepository.saveAndFlush(userAdd);

        // Get the userAdd
        restUserAddMockMvc
            .perform(get(ENTITY_API_URL_ID, userAdd.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userAdd.getId().intValue()))
            .andExpect(jsonPath("$.account").value(DEFAULT_ACCOUNT.doubleValue()));
    }

    @Test
    @Transactional
    void getNonExistingUserAdd() throws Exception {
        // Get the userAdd
        restUserAddMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingUserAdd() throws Exception {
        // Initialize the database
        userAddRepository.saveAndFlush(userAdd);

        int databaseSizeBeforeUpdate = userAddRepository.findAll().size();

        // Update the userAdd
        UserAdd updatedUserAdd = userAddRepository.findById(userAdd.getId()).get();
        // Disconnect from session so that the updates on updatedUserAdd are not directly saved in db
        em.detach(updatedUserAdd);
        updatedUserAdd.account(UPDATED_ACCOUNT);
        UserAddDTO userAddDTO = userAddMapper.toDto(updatedUserAdd);

        restUserAddMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userAddDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userAddDTO))
            )
            .andExpect(status().isOk());

        // Validate the UserAdd in the database
        List<UserAdd> userAddList = userAddRepository.findAll();
        assertThat(userAddList).hasSize(databaseSizeBeforeUpdate);
        UserAdd testUserAdd = userAddList.get(userAddList.size() - 1);
        assertThat(testUserAdd.getAccount()).isEqualTo(UPDATED_ACCOUNT);
    }

    @Test
    @Transactional
    void putNonExistingUserAdd() throws Exception {
        int databaseSizeBeforeUpdate = userAddRepository.findAll().size();
        userAdd.setId(count.incrementAndGet());

        // Create the UserAdd
        UserAddDTO userAddDTO = userAddMapper.toDto(userAdd);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserAddMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userAddDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userAddDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserAdd in the database
        List<UserAdd> userAddList = userAddRepository.findAll();
        assertThat(userAddList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchUserAdd() throws Exception {
        int databaseSizeBeforeUpdate = userAddRepository.findAll().size();
        userAdd.setId(count.incrementAndGet());

        // Create the UserAdd
        UserAddDTO userAddDTO = userAddMapper.toDto(userAdd);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserAddMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userAddDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserAdd in the database
        List<UserAdd> userAddList = userAddRepository.findAll();
        assertThat(userAddList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamUserAdd() throws Exception {
        int databaseSizeBeforeUpdate = userAddRepository.findAll().size();
        userAdd.setId(count.incrementAndGet());

        // Create the UserAdd
        UserAddDTO userAddDTO = userAddMapper.toDto(userAdd);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserAddMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userAddDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserAdd in the database
        List<UserAdd> userAddList = userAddRepository.findAll();
        assertThat(userAddList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateUserAddWithPatch() throws Exception {
        // Initialize the database
        userAddRepository.saveAndFlush(userAdd);

        int databaseSizeBeforeUpdate = userAddRepository.findAll().size();

        // Update the userAdd using partial update
        UserAdd partialUpdatedUserAdd = new UserAdd();
        partialUpdatedUserAdd.setId(userAdd.getId());

        partialUpdatedUserAdd.account(UPDATED_ACCOUNT);

        restUserAddMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserAdd.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserAdd))
            )
            .andExpect(status().isOk());

        // Validate the UserAdd in the database
        List<UserAdd> userAddList = userAddRepository.findAll();
        assertThat(userAddList).hasSize(databaseSizeBeforeUpdate);
        UserAdd testUserAdd = userAddList.get(userAddList.size() - 1);
        assertThat(testUserAdd.getAccount()).isEqualTo(UPDATED_ACCOUNT);
    }

    @Test
    @Transactional
    void fullUpdateUserAddWithPatch() throws Exception {
        // Initialize the database
        userAddRepository.saveAndFlush(userAdd);

        int databaseSizeBeforeUpdate = userAddRepository.findAll().size();

        // Update the userAdd using partial update
        UserAdd partialUpdatedUserAdd = new UserAdd();
        partialUpdatedUserAdd.setId(userAdd.getId());

        partialUpdatedUserAdd.account(UPDATED_ACCOUNT);

        restUserAddMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserAdd.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserAdd))
            )
            .andExpect(status().isOk());

        // Validate the UserAdd in the database
        List<UserAdd> userAddList = userAddRepository.findAll();
        assertThat(userAddList).hasSize(databaseSizeBeforeUpdate);
        UserAdd testUserAdd = userAddList.get(userAddList.size() - 1);
        assertThat(testUserAdd.getAccount()).isEqualTo(UPDATED_ACCOUNT);
    }

    @Test
    @Transactional
    void patchNonExistingUserAdd() throws Exception {
        int databaseSizeBeforeUpdate = userAddRepository.findAll().size();
        userAdd.setId(count.incrementAndGet());

        // Create the UserAdd
        UserAddDTO userAddDTO = userAddMapper.toDto(userAdd);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserAddMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, userAddDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userAddDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserAdd in the database
        List<UserAdd> userAddList = userAddRepository.findAll();
        assertThat(userAddList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchUserAdd() throws Exception {
        int databaseSizeBeforeUpdate = userAddRepository.findAll().size();
        userAdd.setId(count.incrementAndGet());

        // Create the UserAdd
        UserAddDTO userAddDTO = userAddMapper.toDto(userAdd);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserAddMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userAddDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserAdd in the database
        List<UserAdd> userAddList = userAddRepository.findAll();
        assertThat(userAddList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamUserAdd() throws Exception {
        int databaseSizeBeforeUpdate = userAddRepository.findAll().size();
        userAdd.setId(count.incrementAndGet());

        // Create the UserAdd
        UserAddDTO userAddDTO = userAddMapper.toDto(userAdd);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserAddMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(userAddDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserAdd in the database
        List<UserAdd> userAddList = userAddRepository.findAll();
        assertThat(userAddList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteUserAdd() throws Exception {
        // Initialize the database
        userAddRepository.saveAndFlush(userAdd);

        int databaseSizeBeforeDelete = userAddRepository.findAll().size();

        // Delete the userAdd
        restUserAddMockMvc
            .perform(delete(ENTITY_API_URL_ID, userAdd.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserAdd> userAddList = userAddRepository.findAll();
        assertThat(userAddList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
